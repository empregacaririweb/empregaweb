<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Catalogo Cariri</title>

		<!-- INCLUDE STYLESHEET -->
		<link rel="stylesheet" href="stylesheests/cadCandidato.css" />
		<link rel="stylesheet" href="stylesheests/style.css" />
		<link rel="stylesheet" href="stylesheests/bootstrap.min.css" />
		<style>
			body {
				font-family: Arial, Verdana, sans-serif;
				font-size: 13px;
			}
			.ui-autocomplete {
				padding: 0;
				list-style: none;
				background-color: #fff;
				width: 218px;
				border: 1px solid #B0BECA;
				max-height: 350px;
				overflow-x: hidden;
			}
			.ui-autocomplete .ui-menu-item {
				border-top: 1px solid #B0BECA;
				display: block;
				padding: 4px 6px;
				color: #353D44;
				cursor: pointer;
			}
			.ui-autocomplete .ui-menu-item:first-child {
				border-top: none;
			}
			.ui-autocomplete .ui-menu-item.ui-state-focus {
				background-color: #D5E5F4;
				color: #161A1C;
			}
		</style>

		<script src="javascripts/jquery.min.js"></script>
		<script src="javascripts/jquery-ui.min.js"></script>
		<script src="javascripts/jquery.select-to-autocomplete.js"></script>
		<script type="text/javascript">
			(function($) {
				$(function() {
					$('select').selectToAutocomplete();
				});
			})(jQuery);
			$(document).ready(function() {
			$('#bt1').on('click', function() {
				$("#form2").show();
				$("#form1").hide();
				$("#form3").hide();
				});
			$('#bt2').on('click', function() {
				$("#form1").show();
				$("#form2").hide();
				$("#form3").hide();
				});
			$('#bt3').on('click', function() {
				$("#form3").show();
				$("#form2").hide();
				$("#form1").hide();
				});
			$('#bt4').on('click', function() {
				$("#form2").show();
				$("#form1").hide();
				$("#form3").hide();
				});
			});
			function validarNome() {
			
				}
			
		</script>
		<link rel="stylesheet" href="stylesheests/jquery-ui.css" />
	</head>
	<body>

		<?php include "topo.php" ?>

		<div id="container">
			<h2 style="font-weight: bold"><span style="margin-left: 175px">Cadastro do Candidato</span></h2><!-- fim do da frase de anúncio -->
			<div id="retorno"></div>
			<div class="row" id="curriculo">
			<section id="wrapper-products">
				<br/>
				<form role="form" id="formCandidato" name="formCandidato" action="" method="POST" enctype="multipart/form-data" style="background: #FFFFFF; margin-left: -245px">					
					<div id="form1">
						<h4 style="font-weight: bold; margin-left: -1px;" ><span class="pg_atual">Informações de Acesso<span class="pg_dif">Informações Pessoais</span><span class="pg_dif">Informações para Contato</span></h4>
						<div>
						<label for="nome" style="font-weight: bold">Nome Completo:</label>
						<input type="text" name="nome" id="nome" required="required"/>
					</div><!-- Fim da div nome acesso -->
					
					<div>
						<label for="email" style="font-weight: bold">Email:</label>
						<input type="email" name="email" id="email" required="required"/>
					</div><!-- Fim da div email acesso-->
					
					<div>
						<label for="senha" style="font-weight: bold">Senha:</label>
						<input type="password" name="senha" id="senha" required="required"/>
					</div><!-- Fim da div senha acesso -->
						<a class="btn btn-primary" id="bt1" onClick="validarNome()">Proximo</a>
					</div>
					
					<div id="form2" style="display:none;">
						<h4 style="font-weight: bold; margin-left: -1px;" ><span class="pg_dif">Informações de Acesso<span class="pg_atual">Informações Pessoais</span><span class="pg_dif">Informações para Contato</span></h4>
						<div>
						<label for="cpf" style="font-weight: bold">CPF:</label>
						<input type="text" name="cpf" id="cpf" autofocus="autofocus" required="required"/>
					</div><!-- Fim da div CPF candidato -->

					<div>
						<label style="font-weight: bold">Data de Nascimento:</label>
						<input type="number" name="dtNasc01" id="dtNasc01" required="required" min="1" max="31" />
						/
						<input type="number" name="dtNasc02" id="dtNasc02" required="required" min="1" max="12"/>
						/
						<input type="number" name="dtNasc03" id="dtNasc03" required="required" min="1960" max="<?php echo date("Y");?>"/>
					</div><!-- Fim da div data nascimento candidato -->

					<div>
						<label for="estCivil" style="font-weight: bold">Estado Civil:</label>
						<select name="estCivil" id="country-selector" class="estCivil" placeholder="Selecione" autocorrect="off" autocomplete="off" required="required">
							<option value="Solteiro(a)" selected="selected">Solteiro(a)</option>
							<option value="Casado(a)" selected="selected">Casado(a)</option>
							<option value="Divorciado(a)" selected="selected">Divorciado(a)</option>
							<option value="Viúvo(a)" selected="selected">Viúvo(a)</option>
							<option value="Separado(a)" selected="selected">Separado(a)</option>
							<option value="Companheiro(a)" selected="selected">Companheiro(a)</option>
						</select>
					</div><!-- Fim da div estado civil candidato -->
					
					<div>
						<label for="sexo" style="font-weight: bold">Sexo:</label>
						<input type="radio" name="sexo" id="M" value="M"/>
						Masculino
						<input type="radio" name="sexo" id="F" value="F"/>
						Feminino
					</div><!-- Fim da div sexo candidato -->
					
					<br />
					
					<div>
						<label for="empregado" style="font-weight: bold">Empregado:</label>
						<input type="radio" name="emp" id="S" value="S"/>
						Sim
						<input type="radio" name="emp" id="N" value="N"/>
						Não
					</div>
						<a class="btn btn-primary" id="bt2">Anterior</a>
						<a class="btn btn-primary" id="bt3">Proximo</a>
					</div>
					
					<div id="form3" style="display:none;">
						<h4 style="font-weight: bold; margin-left: -1px;" ><span class="pg_dif">Informações de Acesso<span class="pg_dif">Informações Pessoais</span><span class="pg_atual">Informações para Contato</span></h4>
					<div>
						<label for="cidade" style="font-weight: bold">Cidade:</label>
						<select name="cidade" id="country-selector" class="cidade" autocorrect="off" autocomplete="off" >
							<option value="" selected="selected"></option>
                                    <?php
	                                    $listaCidade = $pdo->prepare("SELECT * FROM cidade");
	                                    $listaCidade->execute();
	                                    $todasCidade = $listaCidade->fetchAll(PDO::FETCH_OBJ);
	                                    $contaCidade = $listaCidade->rowCount();
	                                    if ($contaCidade == 0) {
                                        ?>
                                        <option value=""></option>
                                        <?php
										} else {
										foreach ($todasCidade as $dadosCidade):
                                            ?>
                                            <option value="<?php echo $dadosCidade -> idCIDADE; ?>"><?php echo $dadosCidade -> NM_CIDADE; ?></option>		
                                            <?php
											endforeach;
											}
                                    ?>
						</select>
					</div>
					<div>
						<label for="bairro" style="font-weight: bold">Bairro:</label>
						<input type="text" name="bairro" id="bairro" required="required" />
					</div><!-- Fim da div cidade contato -->
					
					<!-- Fim da div bairro contato -->
					
					<div>
						<label for="endereco" style="font-weight: bold">Endereço:</label>
						<input type="text" name="endereco" id="endereco" required="required"/>
					</div><!-- Fim da div endereço contato -->
					
					<div>
						<label for="numero" style="font-weight: bold">Número:</label>
						<input type="text" name="numero" id="numero" required="required"/>
					</div><!-- Fim da div numero contato -->
					
					<div>
						<label for="complemento" style="font-weight: bold">Complemento:</label>
						<input type="text" name="complemento" id="complemento" required="required"/>
					</div><!-- Fim da div complemento contato -->
					
					<div>
						<label for="cep" style="font-weight: bold">CEP:</label>
						<input type="text" name="cep" id="cep" required="required"/>
					</div><!-- Fim da div cep contato -->
					
					<div>
						<label for="celular" style="font-weight: bold">Celular:</label>
						<input type="text" name="celular" id="celular" required="required"/>
					</div><!-- Fim da div celular contato -->
					
					<div>
						<label for="telFixo" style="font-weight: bold">Telefone Fixo:</label>
						<input type="text" name="telFixo" id="telFixo" required="required"/>
					</div><!-- Fim da div telefone contato -->
						<a class="btn btn-primary" id="bt4">Anterior</a>
						<button type="submit" name="cadastrarProximo" id="cadastrarProximo" class="btn btn-primary">Próximo</button>
					</div>
						
					</div>
				</form>

			</div><!-- fim do curriculo que faz a intereção com o form -->
		</div><!-- fim do container -->

		<!-- BEGIN FOOTER -->
		<?php include "rodape.php" ?>
		<!-- END FOOTER -->

		<!-- INCLUDE JAVASCRIPTS -->
		<script src="javascripts/jquery.min.js"></script>
		<script src="javascripts/cadastroCurriculoFormPessoal.js"></script>
		<script src="javascripts/jquery.maskedinput.min.js"></script>
		<script src="javascripts/bootstrap.min.js"></script>
		<script src="javascripts/holder.js"></script>
		<script src="javascripts/scripts.site.js"></script>
	</body>
</html>