<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Catalogo Cariri</title>

		<!-- INCLUDE STYLESHEET -->
		<link rel="stylesheet" href="stylesheests/cadCurriculo.css" />
		<link rel="stylesheet" href="stylesheests/style.css" />
		<link rel="stylesheet" href="stylesheests/bootstrap.min.css" />
		<style>
			body {
				font-family: Arial, Verdana, sans-serif;
				font-size: 13px;
			}
			.ui-autocomplete {
				padding: 0;
				list-style: none;
				background-color: #fff;
				width: 218px;
				border: 1px solid #B0BECA;
				max-height: 350px;
				overflow-x: hidden;
			}
			.ui-autocomplete .ui-menu-item {
				border-top: 1px solid #B0BECA;
				display: block;
				padding: 4px 6px;
				color: #353D44;
				cursor: pointer;
			}
			.ui-autocomplete .ui-menu-item:first-child {
				border-top: none;
			}
			.ui-autocomplete .ui-menu-item.ui-state-focus {
				background-color: #D5E5F4;
				color: #161A1C;
			}
		</style>

		<script src="javascripts/jquery.min.js"></script>
		<script src="javascripts/jquery-ui.min.js"></script>
		<script src="javascripts/jquery.select-to-autocomplete.js"></script>
		<script type="text/javascript">
			(function($) {
				$(function() {
					$('select').selectToAutocomplete();
				});
			})(jQuery);
		</script>
		<link rel="stylesheet" href="stylesheests/jquery-ui.css" />
	</head>
	<body>

		<?php include "topo.php" ?>

		<div id="container">
			<h2 style="font-weight: bold"><span style="margin-left: 175px">Cadastro do Currículo</span></h2><!-- fim do da frase de anúncio -->
			<div id="retorno"></div>
			<div class="row" id="curriculo">
			<section id="wrapper-products">
				<form role="form" id="formCandidato" name="formCandidato" action="" method="POST" enctype="multipart/form-data" style="background: #FFFFFF; margin-left: -245px">					
					<h4 style="font-weight: bold; margin-left: -1px;">Informações Pessoais&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="cadastro_curriculo_adicional.php"><span id="adicionais">Informações Adicionais</span></a></h4>
					<div>
						<label for="cpf" style="font-weight: bold">CPF:</label>
						<input type="text" name="cpf" id="cpf" autofocus="autofocus" required="required"/>
					</div><!-- Fim da div CPF candidato -->

					<div>
						<label style="font-weight: bold">Data de Nascimento:</label>
						<select name="dtNasc01" id="country-selector" class="dtNasc01" autocorrect="off" autocomplete="off" required="required">
							<option value="01" selected="selected">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
						</select>
						/
						<select name="dtNasc02" id="country-selector" class="dtNasc02" autocorrect="off" autocomplete="off" required="required">
							<option value="01" selected="selected">01</option>
							<option value="02">02</option>
						</select>
						/
						<select name="dtNasc03" id="country-selector" class="dtNasc03" autocorrect="off" autocomplete="off" required="required">
							<option value="2016" selected="selected">2016</option>
							<option value="2016">2015</option>
						</select>
					</div><!-- Fim da div data nascimento candidato -->

					<div>
						<label for="estCivil" style="font-weight: bold">Estado Civil:</label>
						<select name="estCivil" id="country-selector" class="estCivil" placeholder="Selecione" autocorrect="off" autocomplete="off" required="required">
							<option value="Solteiro" selected="selected">Solteiro</option>
							<option value="Casado" selected="selected">Casado</option>
						</select>
					</div><!-- Fim da div estado civil candidato -->
					
					<div>
						<label for="sexo" style="font-weight: bold">Sexo:</label>
						<input type="radio" name="sexo" id="M" value="M"/>
						Masculino
						<input type="radio" name="sexo" id="F" value="F"/>
						Feminino
					</div><!-- Fim da div sexo candidato -->
					
					<br />
					
					<div>
						<label for="empregado" style="font-weight: bold">Empregado:</label>
						<input type="radio" name="emp" id="S" value="S"/>
						Sim
						<input type="radio" name="emp" id="N" value="N"/>
						Não
					</div><!-- Fim da div empregado canditato -->
					
					<br />
					
					<h4 id="infContato"><span id="Ic">Contatos</span></h3>
					
					<div>
						<label for="cidade" style="font-weight: bold">Cidade:</label>
						<input type="text" name="cidade" id="cidade" required="required" />
					</div><!-- Fim da div cidade contato -->
					
					<div>
						<label for="bairro" style="font-weight: bold">Bairro:</label>
						<select name="bairro" id="country-selector" class="dtNasc03" autocorrect="off" autocomplete="off" required="required">
							<option value="1" selected="selected">CENTRO</option>
							<option value="2">Aeroporto</option>
						</select>
					</div><!-- Fim da div bairro contato -->
					
					<div>
						<label for="endereco" style="font-weight: bold">Endereço:</label>
						<input type="text" name="endereco" id="endereco" required="required"/>
					</div><!-- Fim da div endereço contato -->
					
					<div>
						<label for="numero" style="font-weight: bold">Número:</label>
						<input type="text" name="numero" id="numero" required="required"/>
					</div><!-- Fim da div numero contato -->
					
					<div>
						<label for="complemento" style="font-weight: bold">Complemento:</label>
						<input type="text" name="complemento" id="complemento" required="required"/>
					</div><!-- Fim da div complemento contato -->
					
					<div>
						<label for="cep" style="font-weight: bold">CEP:</label>
						<input type="text" name="cep" id="cep" required="required"/>
					</div><!-- Fim da div cep contato -->
					
					<div>
						<label for="celular" style="font-weight: bold">Celular:</label>
						<input type="text" name="celular" id="celular" required="required"/>
					</div><!-- Fim da div celular contato -->
					
					<div>
						<label for="telFixo" style="font-weight: bold">Telefone Fixo:</label>
						<input type="text" name="telFixo" id="telFixo" required="required"/>
					</div><!-- Fim da div telefone contato -->
					
					<br />
					
					<h4 id="acesso"><span id="Ac">Acesso</span></h3>
					<div>
						<label for="nome" style="font-weight: bold">Nome Completo:</label>
						<input type="text" name="nome" id="nome" required="required"/>
					</div><!-- Fim da div nome acesso -->
					
					<div>
						<label for="email" style="font-weight: bold">Email:</label>
						<input type="email" name="email" id="email" required="required"/>
					</div><!-- Fim da div email acesso-->
					
					<div>
						<label for="senha" style="font-weight: bold">Senha:</label>
						<input type="password" name="senha" id="senha" required="required"/>
					</div><!-- Fim da div senha acesso -->
					
					<button type="submit" name="cadastrarProximo" id="cadastrarProximo" class="btn btn-primary">Próximo</button>
				</form>

			</div><!-- fim do curriculo que faz a intereção com o form -->
		</div><!-- fim do container -->

		<!-- BEGIN FOOTER -->
		<?php include "rodape.php" ?>
		<!-- END FOOTER -->

		<!-- INCLUDE JAVASCRIPTS -->
		<script src="javascripts/jquery.min.js"></script>
		<script src="javascripts/cadastroCurriculoFormPessoal.js"></script>
		<script src="javascripts/jquery.maskedinput.min.js"></script>
		<script src="javascripts/bootstrap.min.js"></script>
		<script src="javascripts/holder.js"></script>
		<script src="javascripts/scripts.site.js"></script>
	</body>
</html>