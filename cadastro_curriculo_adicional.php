<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Catalogo Cariri</title>

		<!-- INCLUDE STYLESHEET -->
		<link rel="stylesheet" href="stylesheests/cadCurriculo.css" />
		<link rel="stylesheet" href="stylesheests/style.css" />
		<link rel="stylesheet" href="stylesheests/bootstrap.min.css" />
		<style>
			body {
				font-family: Arial, Verdana, sans-serif;
				font-size: 13px;
			}
			.ui-autocomplete {
				padding: 0;
				list-style: none;
				background-color: #fff;
				width: 218px;
				border: 1px solid #B0BECA;
				max-height: 350px;
				overflow-x: hidden;
			}
			.ui-autocomplete .ui-menu-item {
				border-top: 1px solid #B0BECA;
				display: block;
				padding: 4px 6px;
				color: #353D44;
				cursor: pointer;
			}
			.ui-autocomplete .ui-menu-item:first-child {
				border-top: none;
			}
			.ui-autocomplete .ui-menu-item.ui-state-focus {
				background-color: #D5E5F4;
				color: #161A1C;
			}
		</style>

		<script src="javascripts/jquery.min.js"></script>
		<script src="javascripts/jquery-ui.min.js"></script>
		<script src="javascripts/jquery.select-to-autocomplete.js"></script>
		<script type="text/javascript">
			(function($) {
				$(function() {
					$('select').selectToAutocomplete();
				});
			})(jQuery);
		</script>
		<link rel="stylesheet" href="stylesheests/jquery-ui.css" />
	</head>
	<body>

		<?php include "topo.php" ?>

		<div id="container">
			<h2 style="font-weight: bold"><span style="margin-left: 175px">Cadastro do Currículo</span></h2><!-- fim do da frase de anúncio -->
			<div class="row" id="curriculo">
				<div id="retorno"></div>				
				<form role="form" id="formFormacao" name="formFormacao" action="" method="POST" enctype="multipart/form-data" style="background: #FFFFFF; margin-left: -245px">
					<h4 style="font-weight: bold; margin-left: -1px;"><a href="cadastro_curriculo.php">Informações Pessoais</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="active">Informações Adicionais</span></h4>
					<h4 id="forma"><span id="f">Formação</span></h4>
					<div>
						<label for="escolaridade" style="font-weight: bold">Escolaridade:</label>
						<select name="escolaridade" id="country-selector" class="escolaridade" placeholder="Selecione" autocorrect="off" autocomplete="off" required="required">
							<option value="Ensino Fundamental" selected="selected">Ensino Fundamental</option>
							<option value="Ensino Médio" >Ensino Médio</option>
							<option value="Ensino Superior" >Ensino Superior</option>
						</select>
					</div><!-- Fim da div escolaridade -->

					<div>
						<label for="instituicao" style="font-weight: bold">Instituição de Ensino:</label>
						<input type="text" name="instituicao" id="instituicao" required="required" />
					</div><!-- Fim da div instituição -->

					<div>
						<label for="curso" style="font-weight: bold">Curso:</label>
						<input type="text" name="curso" id="curso" required="required" />
					</div><!-- Fim da div curso -->

					<div>
						<label for="conclusao" style="font-weight: bold">Conclusão:</label>
						<input type="number" name="conclusao" id="conclusao" placeholder="Ano de conclusão" required="required" min="1980" max="<?php echo date('Y');?>" />
					</div><!-- Fim da div conclusão -->
					
					<button type="submit" name="cadastrarFormacao" id="cadastrarFormacao" class="btn btn-primary">Cadastrar</button>
				</form>
				
				<br />
				
				<div id="retorno"></div>
				<form role="form" id="formCapacitacao" name="formCapacitacao" action="" method="POST" enctype="multipart/form-data" style="background: #FFFFFF; margin-left: -245px">
					<h4 id="capac"><span id="c">Capacitação</span></h4>
					<div>
						<label for="capacitacao" style="font-weight: bold">Capacitação:</label>
						<input type="text" name="capacitacao" id="capacitacao" required="required" />
					</div><!-- Fim da div capacitação -->
					
					<div>
						<label for="entidade" style="font-weight: bold">Instituição de Ensino:</label>
						<input type="text" name="entidade" id="entidade" required="required" />
					</div><!-- Fim da div instituição capacitaçao -->
					
					<div>
						<label for="cargaH" style="font-weight: bold">Carga Horária:</label>
						<input type="text" name="cargaH" id="cargaH" required="required" />
					</div><!-- Fim da div carga horária capacitação -->
					
					<div>
						<label for="conclusao02" style="font-weight: bold">Conclusão:</label>
						<input type="text" name="conclusao02" id="conclusao02" placeholder="Ano de conclusão" required="required" />
					</div><!-- Fim da div conclusão capacitação -->
					
					<button type="submit" name="cadastrarCapacitacao" id="cadastrarCapacitacao" class="btn btn-primary">Cadastrar</button>
				</form>
				
				<br />
				
				<div id="retorno"></div>
				<form role="form" id="formExperiencia" name="formExperiencia" action="" method="POST" enctype="multipart/form-data" style="background: #FFFFFF; margin-left: -245px">
					<h4 id="exper"><span id="e">Experiência</span></h4>
					<div>
						<label for="empresa" style="font-weight: bold">Empresa:</label>
						<input type="text" name="empresa" id="empresa" required="required" />
					</div><!-- Fim da div empresa experiencia -->
					
					<div>
						<label for="atuacao" style="font-weight: bold">Atuação:</label>
						<input type="text" name="atuacao" id="atuacao" required="required" />
					</div><!-- Fim da div atuação experiência -->
					
					<div>
						<label for="experiencia" style="font-weight: bold">Experiência:</label>
						<input type="text" name="experiencia" id="experiencia" required="required" />
					</div><!-- Fim da div experiência -->
					
					<div>
						<label for="admissao" style="font-weight: bold">Ano de Admissão:</label>
						<input type="text" name="admissao" id="admissao" required="required" />
					</div><!-- Fim da div admissão experiência -->
					
					<div>
						<label for="demissao" style="font-weight: bold">Ano de Demissão:</label>
						<input type="text" name="demissao" id="demissao" required="required" />
					</div><!-- Fim da div demissão experiência -->
					
					<div>
						<label for="descricao" style="font-weight: bold">Descrição sobre você:</label>
						<textarea cols="60" id="descricao" rows="4" name="descricao" maxlength="500" required="required">
							
						</textarea>
					</div><!-- Fim da div descrição experiência -->
					
					<button type="submit" name="cadastrarExperiencia" id="cadastrarExperiencia" class="btn btn-primary">Cadastrar</button>
				</form>
			</div><!-- fim do curriculo que faz a intereção com o form -->
		</div><!-- fim do container -->

		<!-- BEGIN FOOTER -->
		<?php include "rodape.php" ?>
		<!-- END FOOTER -->

		<!-- INCLUDE JAVASCRIPTS -->
		<script src="javascripts/jquery.min.js"></script>
		<script src="javascripts/cadastroAdicionalFormacao.js"></script>
		<script src="javascripts/cadastroAdicionalCapacitacao.js"></script>
		<script src="javascripts/cadastroAdicionalExperiencia.js"></script>
		<script src="javascripts/jquery.maskedinput.min.js"></script>
		<script src="javascripts/bootstrap.min.js"></script>
		<script src="javascripts/holder.js"></script>
		<script src="javascripts/scripts.site.js"></script>
	</body>
</html>