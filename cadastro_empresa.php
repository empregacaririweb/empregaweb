<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Emprega Cariri</title>

		<!-- INCLUDE STYLESHEET -->
		<link rel="stylesheet" href="stylesheests/cadEmpresa.css" />
		<link rel="stylesheet" href="stylesheests/style.css" />
		<link rel="stylesheet" href="stylesheests/bootstrap.min.css" />
		<style>
			body {
				font-family: Arial, Verdana, sans-serif;
				font-size: 13px;
			}
			.ui-autocomplete {
				padding: 0;
				list-style: none;
				background-color: #fff;
				width: 218px;
				border: 1px solid #B0BECA;
				max-height: 350px;
				overflow-x: hidden;
			}
			.ui-autocomplete .ui-menu-item {
				border-top: 1px solid #B0BECA;
				display: block;
				padding: 4px 6px;
				color: #353D44;
				cursor: pointer;
			}
			.ui-autocomplete .ui-menu-item:first-child {
				border-top: none;
			}
			.ui-autocomplete .ui-menu-item.ui-state-focus {
				background-color: #D5E5F4;
				color: #161A1C;
			}
		</style>

		<script src="javascripts/jquery.min.js"></script>
		<script src="javascripts/jquery-ui.min.js"></script>
		<script src="javascripts/jquery.select-to-autocomplete.js"></script>
		<script type="text/javascript">
			(function($) {
				$(function() {
					$('select').selectToAutocomplete();
				});
			})(jQuery);
			$(document).ready(function() {
			$('#bt1').on('click', function() {
				$("#form2").show();
				$("#form1").hide();
				$("#form3").hide();
				});
			$('#bt2').on('click', function() {
				$("#form1").show();
				$("#form2").hide();
				$("#form3").hide();
				});
			$('#bt3').on('click', function() {
				$("#form3").show();
				$("#form2").hide();
				$("#form1").hide();
				});
			$('#bt4').on('click', function() {
				$("#form2").show();
				$("#form1").hide();
				$("#form3").hide();
				});
			});
		</script>
		<link rel="stylesheet" href="stylesheests/jquery-ui.css" />
	</head>
	<body>

		<?php include "topo.php" ?>

		<div id="container">
			<h2 style="font-weight: bold"><span style="margin-left: 175px">Cadastro da Empresa</span></h2><!-- fim do da frase de anúncio -->
			<div id="retorno"></div>
			<div class="row" id="cadastro_emp">
			<section id="wrapper-products">
				<br/>
				<form role="form" id="formEmpresa" name="formEmpresa" action="" method="POST" enctype="multipart/form-data" style="background: #FFFFFF; margin-left: -245px">					
					
					<div id="form1">
						<h4 style="font-weight: bold; margin-left: -1px;" ><span class="pg_atual">Informações de Acesso<span class="pg_dif">Informações Adicionais</span><span class="pg_dif">Informações de Localização</span></h4>
						<div>
							<label for="nomef" style="font-weight: bold">Nome Fantasia:</label>
							<input type="text" name="nomef" id="nomef" required="required"/>
						</div>
					
						<div>
							<label for="email" style="font-weight: bold">Email:</label>
							<input type="email" name="email" id="email" required="required"/>
						</div><!-- Fim da div email acesso-->
					
						<div>
							<label for="senha" style="font-weight: bold">Senha:</label>
							<input type="password" name="senha" id="senha" required="required"/>
						</div><!-- Fim da div senha acesso -->
						<a class="btn btn-primary" id="bt1">Proximo</a>
					</div>
					
					<div id="form2" style="display:none;">
						<h4 style="font-weight: bold; margin-left: -1px;" ><span class="pg_dif">Informações de Acesso<span class="pg_atual">Informações Adicionais</span><span class="pg_dif">Informações de Localização</span></h4>
						<div>
							<label for="rsocial" style="font-weight: bold">Razão Social:</label>
							<input type="text" name="rsocial" id="rsocial" required="required"/>
						</div><!-- Fim da div email acesso-->
					
						<div>
							<label for="cnpj" style="font-weight: bold">CNPJ:</label>
							<input type="text" name="cnpj" id="cnpj" required="required"/>
						</div>
					
						<label for="celular" style="font-weight: bold">Celular:</label>
						<input type="text" name="celular" id="celular" required="required"/>
						<div>
							<label for="telFixo" style="font-weight: bold">Telefone Fixo:</label>
							<input type="text" name="telFixo" id="telFixo" required="required"/>
						</div>
						<a class="btn btn-primary" id="bt2">Anterior</a>
						<a class="btn btn-primary" id="bt3">Proximo</a>
					</div>
					
					<div id="form3" style="display:none;">
						<h4 style="font-weight: bold; margin-left: -1px;" ><span class="pg_dif">Informações de Acesso<span class="pg_dif">Informações Adicionais</span><span class="pg_atual">Informações de Localização</span></h4>
					
					<div>
						<label for="cidade" style="font-weight: bold">Cidade:</label>
						<select name="cidade" id="country-selector" class="cidade" autocorrect="off" autocomplete="off" >
							<option value="" selected="selected"></option>
                                    <?php
	                                    $listaCidade = $pdo->prepare("SELECT * FROM cidade");
	                                    $listaCidade->execute();
	                                    $todasCidade = $listaCidade->fetchAll(PDO::FETCH_OBJ);
	                                    $contaCidade = $listaCidade->rowCount();
	                                    if ($contaCidade == 0) {
                                        ?>
                                        <option value=""></option>
                                        <?php
										} else {
										foreach ($todasCidade as $dadosCidade):
                                            ?>
                                            <option value="<?php echo $dadosCidade -> idCIDADE; ?>"><?php echo $dadosCidade -> NM_CIDADE; ?></option>		
                                            <?php
											endforeach;
											}
                                    ?>
						</select>
					</div>
					<div>
						<label for="bairro" style="font-weight: bold">Bairro:</label>
						<input type="text" name="bairro" id="bairro" required="required" />
					</div>
					
					<div>
						<label for="endereco" style="font-weight: bold">Endereço:</label>
						<input type="text" name="endereco" id="endereco" required="required"/>
					</div><!-- Fim da div endereço contato -->
					<div>
						<label for="numero" style="font-weight: bold">Número:</label>
						<input type="text" name="numero" id="numero" required="required"/>
					</div><!-- Fim da div numero contato -->
					<div>
						<label for="cep" style="font-weight: bold">CEP:</label>
						<input type="text" name="cep" id="cep" required="required"/>
					</div><!-- Fim da div cep contato -->
					<div>
						<a class="btn btn-primary" id="bt4">Anterior</a>
						<button type="submit" name="cadastrarProximo" id="cadastrarProximo" class="btn btn-primary">Próximo</button>
					</div>
						
					</div>
					
					
				</form>

			</div><!-- fim do curriculo que faz a intereção com o form -->
		</div><!-- fim do container -->

		<!-- BEGIN FOOTER -->
		<?php include "rodape.php" ?>
		<!-- END FOOTER -->

		<!-- INCLUDE JAVASCRIPTS -->
		<script src="javascripts/jquery.min.js"></script>
		<script src="javascripts/cadastroEmpresa.js"></script>
		<script src="javascripts/jquery.maskedinput.min.js"></script>
		<script src="javascripts/bootstrap.min.js"></script>
		<script src="javascripts/holder.js"></script>
		<script src="javascripts/scripts.site.js"></script>
	</body>
</html>