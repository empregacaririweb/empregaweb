<?php 
session_start();
$nome = isset($_SESSION['NM_CLIENTE'])? $_SESSION['NM_CLIENTE'] : "Usuário";
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Emprega Cariri - Candidato</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="css/formCandidato.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0" id="menu_topo">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="painel.php"><?php echo $nome;?> - Painel de Controle</a>
			</div>
			<!-- Fim da div header da busca -->

			<!-- icones do topo -->
				<?php
				include_once 'includes/icones-topo.php';
				?>

				<!-- include do menu -->
				<?php
				include_once 'includes/menu.php';
				?>

			</nav>
		<!-- fim da navegação -->

		<div id="page-wrapper">
			<br />
			<div class="row">
				<div class="col-lg-12">
					<form action="" method="post" id="formCapacitacao" name="formCapacitacao" role="form" enctype="multipart/form-data">
											
											<h4 id="acesso"><span>Cadastro de Capacitação</span></h3>
											<div class="col-xs-8">
												<label for="capacitacao">* Capacitação:</label>
												<input type="text" name="capacitacao" id="capacitacao" required="required" class="form-control"/>
											</div>

											<div class="col-xs-4">
												<label for="entidade">* Instituição de Ensino:</label>
												<input type="text" name="entidade" id="entidade" required="required" class="form-control"/>
											</div>

											<div class="col-xs-3">
												<label for="cargaH">* Carga Horária:</label>
												<input type="text" name="cargaH" id="cargaH" required="required" class="form-control"/>
											</div>
											<div class="col-xs-3">
												<label for="conclusao02">* Conclusão:</label>
												<input type="text" name="conclusao02" id="conclusao02" placeholder="Ano de conclusão" required="required" class="form-control"/>
											</div>
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
											<button type="submit" name="cadastrarCapacitacao" id="cadastrarDados" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Cadastrar</button>
											<a href="capacitacao.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
										</form>
				</div>
			</div>
		</div>
		<!-- fim div formulários -->
	</div>
	<!-- fim div conteúdo mae -->

	<!-- jQuery Version 1.11.0 -->
	<script src="js/jquery-1.11.0.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Metis Menu Plugin JavaScript -->
	<script src="js/plugins/metisMenu/metisMenu.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
	<script type="text/javascript" src="js/busca.js"></script>
	<script type="text/javascript" src="js/cadastroAdicionalCapacitacao.js"></script>
</body>
</html>
