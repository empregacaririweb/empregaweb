<?php 
session_start();
$nome = isset($_SESSION['NM_CLIENTE'])? $_SESSION['NM_CLIENTE'] : "Usuário";
include_once ('../funcoes/conexao/conexao.php');
$pdo = conectar();
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Emprega Cariri - Candidato</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="css/formCandidato.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0" id="menu_topo">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="painel.php"><?php echo $nome;?> - Painel de Controle</a>
			</div>
			<!-- Fim da div header da busca -->

			<!-- icones do topo -->
				<?php
				include_once 'includes/icones-topo.php';
				?>

				<!-- include do menu -->
				<?php
				include_once 'includes/menu.php';
				?>

			</nav>
		<!-- fim da navegação -->

		<div id="page-wrapper">
			<br />
			<div class="row">
				<div class="col-lg-12">
					<form action="" method="post" id="formExperiencia" name="formExperiencia" role="form" enctype="multipart/form-data">
											
											<h4 id="acesso"><span>Cadastro de Experiência Profissional</span></h3>
											<div class="col-xs-6">
												<label for="cargo">* Cargo:</label>
												<select name="cargo" id="country-selector" autocorrect="off" autocomplete="off" placeholder="Digite cargo ou área profissional" class="form-control">
													<option value="" selected="selected"></option>
													<?php
														$listaCargo = $pdo->prepare("SELECT * FROM cargo");
														$listaCargo->execute();
														$todasCargo = $listaCargo->fetchAll(PDO::FETCH_OBJ);
														$contaCargo = $listaCargo->rowCount();
	                                    if ($contaCargo == 0) {
                                        ?>
                                        <option value=""></option>
                                        <?php
										} else {
										foreach ($todasCargo as $dadosCargo):
                                            ?>
                                            <option value="<?php echo $dadosCargo -> idCARGO; ?>"><?php echo $dadosCargo -> NM_CARGO; ?></option>		
                                            <?php
											endforeach;
											}
                                    ?>
                                </select></div>
											<div class="col-xs-6">
												<label for="empresa">* Empresa:</label>
												<input type="text" name="empresa" id="empresa" required="required" class="form-control"/>
											</div>

											<div class="col-xs-3">
												<label for="atuacao">* Atuação:</label>
												<input type="text" name="atuacao" id="atuacao" required="required" class="form-control"/>
											</div>

											<div class="col-xs-3">
												<label for="experiencia" style="font-weight: bold">* Experiência:</label>
												<input type="text" name="experiencia" id="experiencia" required="required" class="form-control" />
											</div>
											<div class="col-xs-3">
												<label for="admissao" style="font-weight: bold">* Ano de Admissão:</label>
												<input type="text" name="admissao" id="admissao" required="required" class="form-control"/>
											</div>
											<div class="col-xs-3">
												<label for="demissao" style="font-weight: bold">* Ano de Demissão:</label>
												<input type="text" name="demissao" id="demissao" required="required" class="form-control"/>
											</div>
											<div class="col-xs-12">
												<label for="descricao" style="font-weight: bold">* Descrição sobre você:</label>
												<textarea cols="60" id="descricao" rows="4" name="descricao" maxlength="500" required="required" class="form-control">
												</textarea>
											</div>
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
											<button type="submit" name="cadastrarExperiencia" id="cadastrarDados" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Cadastrar</button>
										</form>
				</div>
			</div>
		</div>
		<!-- fim div formulários -->
	</div>
	<!-- fim div conteúdo mae -->

	<!-- jQuery Version 1.11.0 -->
	<script src="js/jquery-1.11.0.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Metis Menu Plugin JavaScript -->
	<script src="js/plugins/metisMenu/metisMenu.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
	<script type="text/javascript" src="js/busca.js"></script>
	<script type="text/javascript" src="js/cadastroAdicionalExperiencia.js"></script>
</body>
</html>
