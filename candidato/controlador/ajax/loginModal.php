<!doctype html>
<html>
    <head>
        <meta charset="UTF-8"/>

        <style>
            #userlogin, #password{
                width: 70%;
            }
        </style>
    </head>
    <body>

        <!--FORMUL�?RIO DO LOGIN EM MODAL -->
        <?php
        $acao = filter_input(INPUT_POST, 'acao', FILTER_SANITIZE_STRING);

        switch ($acao) {
            case 'form_modal_login':
                ?>
                <div class="row">
                    <div class="painel-body" style="margin-left: 21px">
                        <div id="carregando" class="alert alert-success" style="display:none;"> </div>
                        <form role="form" action="" name="form_modal_login" method="POST" enctype="multipart/form-data">
                            <div class="retorno"></div>
                            <div class="form-group">
                                <label>E-mail</label>
                                <input class="form-control" name="email" id="userlogin" type="email" placeholder="E-mail" />

                                <label>Senha</label>
                                <input class="form-control" name="senha" id="password" type="password" placeholder="Senha" />
                            </div>
                            
                            <button type="submit" class="btn btn-primary" style="width: 80px" name="login">Entrar</button>
                            <a href="#">Esqueceu sua senha?</a>
                        </form>
                        <div id="carregando"> </div>
                    </div>
                </div>
                <?php
                break;
        }
        ?>
        <script src="javascripts/loginModal.js"></script>
    </body>
</html>