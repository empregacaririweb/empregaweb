<?php 
session_start();
$nome = isset($_SESSION['NM_CLIENTE'])? $_SESSION['NM_CLIENTE'] : "Usuário";
include_once ('../funcoes/conexao/conexao.php');
$pdo = conectar();
$consulta = $pdo->query("SELECT * FROM v_candidato where idCLIENTE = '" . $_SESSION['idCLIENTE']."'");
while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
	// aqui eu mostro os valores de minha consulta
	$nomecl = $linha['NM_CLIENTE'];
	$email = $linha['DS_EMAIL'];
	$senha = $linha['DS_SENHA'];
	$cpf = $linha['DS_CPF'];
	$dtNasc = $linha['DT_NASC'];
	$sexo = $linha['SEXO'];	
	$civil = $linha['DS_EST_CIVIL'];
	$cidade = $linha['CIDADE_idCIDADE'];
	$bairro = $linha['BAIRRO'];
	$endereco = $linha['ENDERECO'];
	$numero = $linha['NUMERO'];
	$comp = $linha['DS_COMPLEMENTO'];
	$cep = $linha['DS_CEP'];
	$tel1 = $linha['NUM_TEL1'];
	$tel2 = $linha['NUM_TEL2'];
	$status = $linha['DS_STATUS'];
	
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Emprega Cariri - Candidato</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="css/formCandidato.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="painel.php"><?php echo $nome;?> - Painel de Controle</a>
			</div>
			<!-- Fim da div header da busca -->

			<!-- icones do topo -->
				<?php
				include_once 'includes/icones-topo.php';
				?>

				<!-- include do menu -->
				<?php
				include_once 'includes/menu.php';
				?>

			</nav>
		<!-- fim da navegação -->

		<div id="page-wrapper">
			<br />
			<div class="row">
				<div class="col-lg-12">
					<form action="" method="post" id="formCandidato" name="formCandidato" role="form" enctype="multipart/form-data">
											
											<h4 id="acesso"><span>Acesso</span></h3>
											<div class="col-xs-8">
												<label for="nome">* Nome Completo:</label>
												<input type="text" class="form-control" name="nome" id="nome" autofocus="autofocus" required="required" value="<?php echo $nomecl;?>" />
											</div>

											<div class="col-xs-4">
												<label for="email">* Email:</label>
												<input type="email" name="email" id="email" class="form-control" required="required" value="<?php echo $email;?>"/>
											</div>

											<div class="col-xs-5">
												<label for="senha">* Senha:</label>
												<input type="text" class="form-control" name="senha" id="senha" required="required" value="<?php echo $senha;?>"/>
											</div>
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
											<h4 id="acesso"><span>Informações Pessoais</span></h3>
											<div class="col-xs-5">
												<label for="enderecoCandidato">* CPF:</label>
												<input type="text" name="cpf" id="cpf" class="form-control" required="required" value="<?php echo $cpf;?>"/>
											</div>
											<div class="col-xs-4">
												<label style="font-weight: bold">* Data de Nascimento:</label>
												<input type="text" name="dtnasc" class="form-control" id="dtnasc" required="required" value="<?php echo $dtNasc;?>"/>
												
												</div>
												
												<div class="col-xs-4">
													<label style="font-weight: bold">* Sexo:</label>
													<input type="radio" name="sexo" id="M" value="M" class="form-control"/><label>Masculino</label>
													<input type="radio" name="sexo" id="F" value="F" class="form-control" class="form-control"/>Feminino
												</div>
												
												<div class="col-xs-4">
												<label style="font-weight: bold">* Estado Civil:</label>
													<select name="estCivil" id="country-selector" class="form-control" placeholder="Selecione" autocorrect="off" autocomplete="off" required="required">
														<option value="Solteiro(a)" <?php if($civil == "Solteiro(a)"){echo "selected='selected'"; }?>>Solteiro(a)</option>
														<option value="Casado(a)" <?php if($civil == "Casado(a)"){echo "selected='selected'"; }?>>Casado(a)</option>
														<option value="Divorciado(a)" <?php if($civil == "Divorciado(a)"){echo "selected='selected'"; }?>>Divorciado(a)</option>
														<option value="Viúvo(a)" <?php if($civil == "Viúvo(a)"){echo "selected='selected'"; }?>>Viúvo(a)</option>
														<option value="Separado(a)" <?php if($civil == "Separado(a)"){echo "selected='selected'"; }?>>Separado(a)</option>
														<option value="Companheiro(a)" <?php if($civil == "Companheiro(a)"){echo "selected='selected'"; }?>>Companheiro(a)</option>
													</select>
												</div>
										

										
											
											<button type="submit" name="cadastrarDados" id="cadastrarDados" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Cadastrar</button>
										</form>
				</div>
			</div>
		</div>
		<!-- fim div formulários -->
	</div>
	<!-- fim div conteúdo mae -->

	<!-- jQuery Version 1.11.0 -->
	<script src="js/jquery-1.11.0.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Metis Menu Plugin JavaScript -->
	<script src="js/plugins/metisMenu/metisMenu.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
	<script type="text/javascript" src="js/busca.js"></script>
</body>
</html>
