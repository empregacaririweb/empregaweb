<?php 
session_start();
$nome = isset($_SESSION['NM_CLIENTE'])? $_SESSION['NM_CLIENTE'] : "Usuário";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/modal.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="css/plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="painel.php"><?php echo $nome;?> - Painel de Controle</a>
			</div>
			<!-- Fim da div header da busca -->

			<!-- icones do topo -->
				<?php
				include_once 'includes/icones-topo.php';
				?>

				<!-- include do menu -->
				<?php
				include_once 'includes/menu.php';
				?>

			</nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Minhas Experiências Profissionais</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="cadastroExperiencia.php" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i>Adicionar</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Cargo</th>
                                            <th>Empresa</th>
                                            <th>Adimissão</th>
                                            <th>Demissão</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
											include_once ('../funcoes/conexao/conexao.php');
											$pdo = conectar();
											$consulta = $pdo->query("SELECT * FROM v_experiencia where idCLIENTE = '" . $_SESSION['idCLIENTE']."'");
											while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
												// aqui eu mostro os valores de minha consulta
												$cargo = $linha['NM_CARGO'];
												$empresa = $linha['NM_EMPRESA'];
												$admissao = $linha['DT_ADIMISSAO'];
												$demissao = $linha['DT_DEMISSAO'];
												$id = $linha['idEXPERIENCIA'];
											
										?>
                                        <tr class="odd gradeA">
                                            <td><?php echo $cargo;?></td>
                                            <td><?php echo $empresa;?></td>
                                            <td><?php echo date('d/m/Y',strtotime($admissao));?></td>
                                            <td><?php echo date('d/m/Y',strtotime($demissao));?></td>
                                            <td class="center">
											<a href="editarExperiencia.php?x=<?php echo $id;?>" id="cand"><img src="<?php echo"img/editar.png"; ?>" alt="Cadastro Candidato" border="0" onmouseover="this.src='<?php echo"img/editar.png"; ?>'" onmouseout="this.src='<?php echo"img/editar.png"; ?>'"/></a>
											<a href="?x=<?php echo $id;?>&#abrirRemocaoModal" id="cand"><img src="<?php echo"img/fechar.png"; ?>" alt="Cadastro Candidato" border="0" onmouseover="this.src='<?php echo"img/fechar.png"; ?>'" onmouseout="this.src='<?php echo"img/fechar.png"; ?>'"/></a>
											</td>
                                        </tr>
										
										<?php
											}
										?>
                                        
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
		<div id="abrirRemocaoModal" class="janelaModal">
					<div>
						<form id="deleteExperiencia" name="deleteExperiencia" method="post">
						<div class="cabecarioModal">
							<?php $_SESSION['id_reg'] = $_GET['x']; ?>
							<h2>Deseja excluir esse Registro?</h2>
							<a href="#fechar" title="Fechar" class="fechar">X</a>
						</div>
						<div class="conteudoModal">
							<button type="submit" name="cadastrarCapacitacao" id="cadastrarDados" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Sim</button>
							<a href="#fechar" class="btn btn-danger"><i class="fa fa-times"></i> Não</a>	
						</div>
						</form>
					</div>
				</div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>
	<script src="js/exclusaoExperiencia.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>
