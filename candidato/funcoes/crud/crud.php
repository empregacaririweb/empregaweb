<?php

//FUNÇÃO DE CADASTRO DO FORMULÁRIO DO CANDIDATO
function addCadastroUsuario($nome, $sobrenome, $email, $senha) {
    $pdo = conectar();

    try {
        $cadastro = $pdo->prepare("INSERT INTO usuario (nm_nome, nm_sobrenome, ds_email, ds_senha) VALUES (?,?,?,?)") or die(mysql_erro());
        $cadastro->bindValue(1, $nome, PDO::PARAM_STR);
        $cadastro->bindValue(2, $sobrenome, PDO::PARAM_STR);
        $cadastro->bindValue(3, $email, PDO::PARAM_STR);
        $cadastro->bindValue(4, $senha, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}

//FUNÇÃO DE CADASTRO DO FORMULÁRIO DE CURRÍCULO DO CANDIDATO
function addCadastroPessoais($cpf, $dataNascimento01, $dataNascimento02, $dataNascimento03, $estCivil, $sexo, $emp, $cidade, $bairro, $endereco, $numero, $complemento, $cep, $celular, $telFixo, $nomeCompleto, $email, $senha) {
	$status = "C";
	$dt_nasc = "$dataNascimento03-$dataNascimento02-$dataNascimento01";
    $pdo = conectar();
    try {
        $cadastro = $pdo->prepare("CALL spi_candidato(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)") or die(mysql_erro());
        $cadastro->bindValue(1, $nomeCompleto, PDO::PARAM_STR);
        $cadastro->bindValue(2, $email, PDO::PARAM_STR);
        $cadastro->bindValue(3, $senha, PDO::PARAM_STR);
        $cadastro->bindValue(4, $status, PDO::PARAM_STR);
        $cadastro->bindValue(5, $cpf, PDO::PARAM_STR);
        $cadastro->bindValue(6, $dt_nasc, PDO::PARAM_STR);
        $cadastro->bindValue(7, $sexo, PDO::PARAM_STR);
        $cadastro->bindValue(8, $estCivil, PDO::PARAM_STR);
		$cadastro->bindValue(9, $cidade, PDO::PARAM_STR);
		$cadastro->bindValue(10, $bairro, PDO::PARAM_STR);
        $cadastro->bindValue(11, $endereco, PDO::PARAM_STR);
        $cadastro->bindValue(12, $numero, PDO::PARAM_STR);
        $cadastro->bindValue(13, $complemento, PDO::PARAM_STR);
        $cadastro->bindValue(14, $cep, PDO::PARAM_STR);
        $cadastro->bindValue(15, $celular, PDO::PARAM_STR);
        $cadastro->bindValue(16, $telFixo, PDO::PARAM_STR);
        $cadastro->bindValue(17, $emp, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}

function addCadastroEmpresa($nomef, $email, $senha, $rsocial, $cnpj, $celular, $telFixo, $cidade, $bairro, $endereco, $numero,$cep) {
    $pdo = conectar();
	$status = "E";
    try {
        $cadastro = $pdo->prepare("CALL spi_empresa(?,?,?,?,?,?,?,?,?,?,?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $nomef, PDO::PARAM_STR);
        $cadastro->bindValue(2, $email, PDO::PARAM_STR);
        $cadastro->bindValue(3, $senha, PDO::PARAM_STR);
        $cadastro->bindValue(4, $status, PDO::PARAM_STR);
        $cadastro->bindValue(5, $nomef, PDO::PARAM_STR);
        $cadastro->bindValue(6, $rsocial, PDO::PARAM_STR);
        $cadastro->bindValue(7, $cnpj, PDO::PARAM_STR);
		$cadastro->bindValue(8, $cidade, PDO::PARAM_STR);
		$cadastro->bindValue(9, $bairro, PDO::PARAM_STR);
        $cadastro->bindValue(10, $endereco, PDO::PARAM_STR);
        $cadastro->bindValue(11, $numero, PDO::PARAM_STR);
        $cadastro->bindValue(12, $cep, PDO::PARAM_STR);
        $cadastro->bindValue(13, $celular, PDO::PARAM_STR);
        $cadastro->bindValue(14, $telFixo, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}
function addCadastroAdicionalCapacitacao($capacitacao, $entidade, $cargaH, $conclusao02) {
    $pdo = conectar();
	$id_cliente = $_SESSION['idCLIENTE'];
    try {
        $cadastro = $pdo->prepare("CALL spi_capacitacao(?,?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $id_cliente, PDO::PARAM_INT);
        $cadastro->bindValue(2, $capacitacao, PDO::PARAM_STR);
        $cadastro->bindValue(3, $entidade, PDO::PARAM_STR);
        $cadastro->bindValue(4, $cargaH, PDO::PARAM_STR);
		$cadastro->bindValue(5, $conclusao02, PDO::PARAM_INT);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}

function editCadastroAdicionalCapacitacao($id_cliente,$id_cap,$capacitacao, $entidade, $cargaH, $conclusao02) {
    $pdo = conectar();
	
    try {
        $cadastro = $pdo->prepare("CALL spu_capacitacao(?,?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $id_cap, PDO::PARAM_INT);
		$cadastro->bindValue(2, $capacitacao, PDO::PARAM_STR);
        $cadastro->bindValue(3, $entidade, PDO::PARAM_STR);
        $cadastro->bindValue(4, $cargaH, PDO::PARAM_STR);
		$cadastro->bindValue(5, $conclusao02, PDO::PARAM_INT);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
	
}
function deleteCadastroAdicionalCapacitacao($id_cliente,$id_cap) {
    $pdo = conectar();
	
    try {
        $cadastro = $pdo->prepare("CALL spd_capacitacao(?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $id_cap, PDO::PARAM_INT);
		$cadastro->bindValue(2, $id_cliente, PDO::PARAM_INT);
        
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
	
}
function addCadastroAdicionalFormacao($escolaridade, $instituicao, $curso, $conclusao) {
    $pdo = conectar();
	session_start();
	$id_cliente = $_SESSION['idCLIENTE'];
    try {
        $cadastro = $pdo->prepare("CALL spi_formacao(?,?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $id_cliente, PDO::PARAM_INT);
        $cadastro->bindValue(2, $escolaridade, PDO::PARAM_STR);
        $cadastro->bindValue(3, $instituicao, PDO::PARAM_STR);
        $cadastro->bindValue(4, $curso, PDO::PARAM_STR);
		$cadastro->bindValue(5, $conclusao, PDO::PARAM_INT);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}
function editFormacao($id_form, $escolaridade, $instituicao, $curso, $conclusao) {
    $pdo = conectar();
	session_start();
	try {
        $cadastro = $pdo->prepare("CALL spu_formacao(?,?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $id_form, PDO::PARAM_INT);
        $cadastro->bindValue(2, $escolaridade, PDO::PARAM_STR);
        $cadastro->bindValue(3, $instituicao, PDO::PARAM_STR);
        $cadastro->bindValue(4, $curso, PDO::PARAM_STR);
		$cadastro->bindValue(5, $conclusao, PDO::PARAM_INT);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}
function deleteFormacao($id_exp,$id_cliente) {
    $pdo = conectar();
	
    try {
        $cadastro = $pdo->prepare("CALL spd_formacao(?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $id_exp, PDO::PARAM_INT);
		$cadastro->bindValue(2, $id_cliente, PDO::PARAM_INT);
        
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
	
}
function addCadastroAdicionalExperiencia($cargo,$empresa, $atuacao, $experiencia, $admissao, $demissao, $descricao) {
    $pdo = conectar();
	$id_cliente = $_SESSION['idCLIENTE'];
	$dataadmissao = implode("-",array_reverse(explode("/",$admissao))); //converter data pro formato do mysql
	$datademissao = implode("-",array_reverse(explode("/",$demissao)));
    try {
        $cadastro = $pdo->prepare("CALL spi_experiencia(?,?,?,?,?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $cargo, PDO::PARAM_INT);
        $cadastro->bindValue(2, $id_cliente, PDO::PARAM_INT);
        $cadastro->bindValue(3, $empresa, PDO::PARAM_STR);
        $cadastro->bindValue(4, $atuacao, PDO::PARAM_STR);
		$cadastro->bindValue(5, $experiencia, PDO::PARAM_STR);
		$cadastro->bindValue(6, $admissao, PDO::PARAM_STR);
		$cadastro->bindValue(7, $demissao, PDO::PARAM_STR);
		$cadastro->bindValue(8, $descricao, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}
function editExperiencia($id_exp,$cargo,$empresa, $atuacao, $experiencia, $admissao, $demissao, $descricao) {
    $pdo = conectar();
	/* $id_cliente = $_SESSION['idCLIENTE'];
	$dataadmissao = implode("-",array_reverse(explode("/",$admissao))); //converter data pro formato do mysql
	$datademissao = implode("-",array_reverse(explode("/",$demissao))); */
    try {
        $cadastro = $pdo->prepare("CALL spu_experiencia(?,?,?,?,?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $id_exp, PDO::PARAM_INT);
		$cadastro->bindValue(2, $cargo, PDO::PARAM_INT);
        $cadastro->bindValue(3, $empresa, PDO::PARAM_STR);
        $cadastro->bindValue(4, $atuacao, PDO::PARAM_STR);
		$cadastro->bindValue(5, $experiencia, PDO::PARAM_STR);
		$cadastro->bindValue(6, $admissao, PDO::PARAM_STR);
		$cadastro->bindValue(7, $demissao, PDO::PARAM_STR);
		$cadastro->bindValue(8, $descricao, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}
function deleteExperiencia($id_exp) {
    $pdo = conectar();
	
    try {
        $cadastro = $pdo->prepare("CALL spd_experiencia(?)") or die(mysql_error());
        $cadastro->bindValue(1, $id_exp, PDO::PARAM_INT);
		
        
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
	
}

////FUN��O DE CADASTRO DO NOVO BAIRRO
//function cadastroNovoBairro($novoBairro) {
//    $pdo = conectar();
//
//    try {
//
//        $cadNvBairo = $pdo->prepare("INSERT INTO bairro (nm_bairro) VALUES (?)") or die(mysql_error());
//        $cadNvBairo->bindValue(1, $novoBairro, PDO::PARAM_STR);
//        $cadNvBairo->execute();
//
//        if ($cadNvBairo->rowCount() == 1):
//            return TRUE;
//        else:
//            return FALSE;
//        endif;
//    } catch (PDOExcetion $erro) {
//        echo $erro->getMessage();
//    }
//}
//
////FUN�AO DE CADASTRO DO NOVO ENDERE�O
//function cadastroNovoEndereco($novoEndereco) {
//    $pdo = conectar();
//
//    try {
//
//        $cadNvEndereco = $pdo->prepare("INSERT INTO logradouro (nm_logradouro) VALUES (?)") or die(mysql_error());
//        $cadNvEndereco->bindValue(1, $novoEndereco, PDO::PARAM_STR);
//        $cadNvEndereco->execute();
//
//        if ($cadNvEndereco->rowCount() == 1):
//            return TRUE;
//        else:
//            return FALSE;
//        endif;
//    } catch (PDOExcetion $erro) {
//        echo $erro->getMessage();
//    }
//}
//
////FUN��O DE CADASTRO DO NOVO SERVI�O
//function cadastroNovoServico($novoServico) {
//    $pdo = conectar();
//
//    try {
//
//        $cadNvServico = $pdo->prepare("INSERT INTO tipo_servico (nm_Tipo_Servico) VALUES (?)") or die(mysql_error());
//        $cadNvServico->bindValue(1, $novoServico, PDO::PARAM_STR);
//        $cadNvServico->execute();
//
//        if ($cadNvServico->rowCount() == 1):
//            return TRUE;
//        else:
//            return FALSE;
//        endif;
//    } catch (PDOExcetion $erro) {
//        echo $erro->getMessage();
//    }
//}
//
////FUNCI��O DE CADASTRO DO NOO FISCAL
//function cadastroNovoFiscal($novoFiscal) {
//    $pdo = conectar();
//
//    try {
//
//        $cadNvFiscal = $pdo->prepare("INSERT INTO fiscal (nm_Fiscal) VALUES (?)") or die(mysql_error());
//        $cadNvFiscal->bindValue(1, $novoFiscal, PDO::PARAM_STR);
//        $cadNvFiscal->execute();
//
//        if ($cadNvFiscal->rowCount() == 1):
//            return TRUE;
//        else:
//            return FALSE;
//        endif;
//    } catch (PDOExcetion $erro) {
//        echo $erro->getMessage();
//    }
//}
//
////FUN��O DE UPDATE DO CLIENTE
//function atualizarCliente($selecionaCliente, $dataVisita, $tipo_servico, $fiscal, $dataCadastro, $regiao, $semana, $status, $Obs) {
//    $pdo = conectar();
//    $id = addslashes($_GET['id_Cliente']);
//    try {
//
//        $up = $pdo->prepare("UPDATE agendamento SET id_Cliente = ?, dt_Visita = ?, id_Tipo_Servico = ?, id_Fiscal = ?, dt_Data = ?, nm_regiao = ?, ds_semanal = ?, ds_Status = ?, ds_Observacao = ? WHERE id_Cliente = ?") or die(mysql_error());
//        $up->bindValue(1, $selecionaCliente, PDO::PARAM_STR);
//        $up->bindValue(2, $dataVisita, PDO::PARAM_STR);
//        $up->bindValue(3, $tipo_servico, PDO::PARAM_STR);
//        $up->bindValue(4, $fiscal, PDO::PARAM_STR);
//        $up->bindValue(5, $dataCadastro, PDO::PARAM_STR);
//        $up->bindValue(6, $regiao, PDO::PARAM_STR);
//        $up->bindValue(7, $semana, PDO::PARAM_STR);
//        $up->bindValue(8, $status, PDO::PARAM_INT);
//        $up->bindValue(9, $Obs, PDO::PARAM_STR);
//        $up->bindValue(9, $id, PDO::PARAM_STR);
//        $up->execute();
//        if ($up->rowCount() > 0):
//            return $up->fetchAll(PDO::FETCH_ASSOC);
//        else:
//            return FALSE;
//        endif;
//        
//    } catch (PDOExcetion $erro) {
//        echo $erro->getMessage();
//    }
//}
?>
