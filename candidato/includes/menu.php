<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse" id="menu_lat">
		<ul class="nav" id="side-menu">

			<li>
				<a href="dadosCadastrais.php" style="color: blue;"><i class="glyphicon glyphicon-edit"></i> Dados Cadastrais</a>
			</li><!-- Fim da div dados -->

			<li>
				<a href="capacitacao.php" style="color: blue;"><i class="glyphicon glyphicon-list-alt"></i> Capacitações</a>
			</li>
			<li>
				<a href="experiencia.php" style="color: blue;"><i class="glyphicon glyphicon-list-alt"></i> Experiência Profissional</a>
			</li>
			<li>
				<a href="formacao.php" style="color: blue;"><i class="glyphicon glyphicon-list-alt"></i> Formação</a>
			</li>
		</ul>
	</div>
</div><!-- fim navbar -->