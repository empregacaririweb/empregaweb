$(document).ready(function() {

	$('form[name="deleteCapacitacao"]').submit(function() {
		var form = $(this);
		var botao = form.find(':button');

		$.ajax({
			url : "../controlador/ajax/controller.php",
			type : "POST",
			data : "acao=exclusaoCapacitacao&" + form.serialize(),
			beforeSend : function() {
				botao.attr('disabled', true);
				$("#loads").fadeIn('slow');
			},
			success : function(retorno) {
				botao.attr('disabled', false);
				$("#loads").fadeOut('slow');
				if (retorno === 'salvou') {
					msg('<center><strong>Cadastrado com sucesso!</strong></center>', 'sucesso');
					$(location).attr('href', 'capacitacao.php');
				} else {
					msg('<center><strong>Ops! Um erro ocorreu!</strong></center>', 'erro');
					alert(retorno);
				}
			}
		});
		return false;
	});

	// FUNÇÕES GERAIS
	function msg(msg, tipo) {
        var retorno = $("#retorno");
        var tipo = (tipo === 'sucesso') ? 'success' : (tipo === 'alerta') ? 'warning' : (tipo === 'erro') ? 'danger' : (tipo === 'info') ? 'info' : alert('INFORME MENSAGEM DE SUCESSO, ALERTA, ERRO E INFO');
		
        retorno.empty().slideUp('fast', function () {
            return $(this).html('<div class="alert alert-' + tipo + '" role="alert">' + msg + '</div>').slideDown('slow');
        });
        setTimeout(function () {
            retorno.slideUp('slow');
        }, 5000);
    }
}); 