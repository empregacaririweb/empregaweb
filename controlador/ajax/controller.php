<?php

ob_start();
session_start();
include "../../funcoes/conexao/conexao.php";
$pdo = conectar();
include "../../funcoes/crud/crud.php";

/**
 * TESTE O LOGIN DO USUÁRIO E AVISA SE O MESMO TEM
 * CONTA, CASO CONTRÁRIO AVISO DE ERRO NO MODAL LOGIN.
 */
$acao = filter_input(INPUT_POST, 'acao', FILTER_SANITIZE_STRING);

switch ($acao) :
	//AÇÃO LOGIN, TESTA O LOGIN DO CANDIDATO
	case 'login' :
		//Faz a interação
		try {
			$login = $_POST['email'];
			$senha = $_POST['senha'];

			if (empty($login) || empty($senha)) :
				echo 'empty';
			else :
				$logar = $pdo -> prepare("SELECT * FROM cliente WHERE DS_EMAIL = ? AND DS_SENHA = ?") or die(mysql_error());
				$logar -> bindValue(1, $login, PDO::PARAM_STR);
				$logar -> bindValue(2, $senha, PDO::PARAM_STR);
				$logar -> execute();
				if ($logar -> rowCount() == 1) :
					 foreach ($logar as $dados){
					$_SESSION['idCLIENTE'] = $dados["idCLIENTE"];
					$_SESSION['NM_CLIENTE'] = $dados["NM_CLIENTE"];
					$_SESSION['DS_STATUS'] = $dados["DS_STATUS"]; 
					}
					if ($_SESSION['DS_STATUS'] == "C"){
						echo 'sucesso-c';
					}else if ($_SESSION['DS_STATUS'] == "E"){
						echo 'sucesso-e';
					}else{
						echo 'sucesso-b';
					}
					
				else :
					echo 'erro';
				endif;
			endif;
		} catch (PDOException $erro) {
			echo $erro -> getMessage();
		}
		break;

	case 'cadastroPessoais' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST, CADASTRO DO CURRICULO
		$cpf = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_NUMBER_INT);
		$dataNascimento01 = filter_input(INPUT_POST, 'dtNasc01', FILTER_SANITIZE_STRING);
		$dataNascimento02 = filter_input(INPUT_POST, 'dtNasc02', FILTER_SANITIZE_STRING);
		$dataNascimento03 = filter_input(INPUT_POST, 'dtNasc03', FILTER_SANITIZE_STRING);
		$estCivil = filter_input(INPUT_POST, 'estCivil', FILTER_SANITIZE_STRING);
		$sexo = filter_input(INPUT_POST, 'sexo', FILTER_SANITIZE_STRING);
		$emp = filter_input(INPUT_POST, 'emp', FILTER_SANITIZE_STRING);
		$cidade = filter_input(INPUT_POST, 'cidade', FILTER_SANITIZE_STRING);
		$bairro = filter_input(INPUT_POST, 'bairro', FILTER_SANITIZE_STRING);
		$endereco = filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING);
		$numero = filter_input(INPUT_POST, 'numero', FILTER_SANITIZE_STRING);
		$complemento = filter_input(INPUT_POST, 'complemento', FILTER_SANITIZE_STRING);
		$cep = filter_input(INPUT_POST, 'cep', FILTER_SANITIZE_STRING);
		$celular = filter_input(INPUT_POST, 'celular', FILTER_SANITIZE_STRING);
		$telFixo = filter_input(INPUT_POST, 'telFixo', FILTER_SANITIZE_STRING);
		$nomeCompleto = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
		$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
		$senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);

		if (addCadastroPessoais($cpf, $dataNascimento01, $dataNascimento02, $dataNascimento03, $estCivil, $sexo, $emp, $cidade, $bairro, $endereco, $numero, $complemento, $cep, $celular, $telFixo, $nomeCompleto, $email, $senha)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;

	case 'cadastroAdicionalFormacao' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$escolaridade = filter_input(INPUT_POST, 'escolaridade', FILTER_SANITIZE_STRING);
		$instituicao = filter_input(INPUT_POST, 'instituicao', FILTER_SANITIZE_STRING);
		$curso = filter_input(INPUT_POST, 'curso', FILTER_SANITIZE_STRING);
		$conclusao = filter_input(INPUT_POST, 'conclusao', FILTER_SANITIZE_NUMBER_INT);

		if (addCadastroAdicionalFormacao($escolaridade, $instituicao, $curso, $conclusao)):
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	case 'alteraFormacao' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$id_form = $_SESSION['id_form'];
		$escolaridade = filter_input(INPUT_POST, 'escolaridade', FILTER_SANITIZE_STRING);
		$instituicao = filter_input(INPUT_POST, 'instituicao', FILTER_SANITIZE_STRING);
		$curso = filter_input(INPUT_POST, 'curso', FILTER_SANITIZE_STRING);
		$conclusao = filter_input(INPUT_POST, 'conclusao', FILTER_SANITIZE_NUMBER_INT);

		if (editFormacao($id_form, $escolaridade, $instituicao, $curso, $conclusao)):
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	case 'exclusaoFormacao' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$id_form = $_SESSION['id_reg'];
		$id_cliente = $_SESSION['idCLIENTE'];
		if (deleteFormacao($id_form,$id_cliente)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	case 'cadastroAdicionalCapacitacao' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$capacitacao = filter_input(INPUT_POST, 'capacitacao', FILTER_SANITIZE_STRING);
		$entidade = filter_input(INPUT_POST, 'entidade', FILTER_SANITIZE_STRING);
		$cargaH = filter_input(INPUT_POST, 'cargaH', FILTER_SANITIZE_NUMBER_INT);
		$conclusao02 = filter_input(INPUT_POST, 'conclusao02', FILTER_SANITIZE_NUMBER_INT);

		if (addCadastroAdicionalCapacitacao($capacitacao, $entidade, $cargaH, $conclusao02)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;

	case 'alteracaoCapacitacao' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$id_cliente = $_SESSION['idCLIENTE'];
		$id_cap = $_SESSION['id'];
		$capacitacao = filter_input(INPUT_POST, 'capacitacao', FILTER_SANITIZE_STRING);
		$entidade = filter_input(INPUT_POST, 'entidade', FILTER_SANITIZE_STRING);
		$cargaH = filter_input(INPUT_POST, 'cargaH', FILTER_SANITIZE_NUMBER_INT);
		$conclusao02 = filter_input(INPUT_POST, 'conclusao02', FILTER_SANITIZE_NUMBER_INT);

		if (editCadastroAdicionalCapacitacao($id_cliente,$id_cap,$capacitacao, $entidade, $cargaH, $conclusao02)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	
	case 'exclusaoCapacitacao' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$id_cliente = $_SESSION['idCLIENTE'];
		$id_cap = $_SESSION['id_reg'];
		if (deleteCadastroAdicionalCapacitacao($id_cliente,$id_cap)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	
	case 'cadastroAdicionalExperiencia' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$cargo = filter_input(INPUT_POST, 'cargo', FILTER_SANITIZE_NUMBER_INT);
		$empresa = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_STRING);
		$atuacao = filter_input(INPUT_POST, 'atuacao', FILTER_SANITIZE_STRING);
		$experiencia = filter_input(INPUT_POST, 'experiencia', FILTER_SANITIZE_STRING);
		$admissao = filter_input(INPUT_POST, 'admissao', FILTER_SANITIZE_STRING);
		$demissao = filter_input(INPUT_POST, 'demissao', FILTER_SANITIZE_STRING);
		$descricao = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);

		if (addCadastroAdicionalExperiencia($cargo,$empresa, $atuacao, $experiencia, $admissao, $demissao, $descricao)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	case 'alteracaoExperiencia' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$id_exp = $_SESSION['id'];
		$cargo = filter_input(INPUT_POST, 'cargo', FILTER_SANITIZE_NUMBER_INT);
		$empresa = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_STRING);
		$atuacao = filter_input(INPUT_POST, 'atuacao', FILTER_SANITIZE_STRING);
		$experiencia = filter_input(INPUT_POST, 'experiencia', FILTER_SANITIZE_STRING);
		$admissao = filter_input(INPUT_POST, 'admissao', FILTER_SANITIZE_STRING);
		$demissao = filter_input(INPUT_POST, 'demissao', FILTER_SANITIZE_STRING);
		$descricao = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);

		if (editExperiencia($id_exp,$cargo,$empresa, $atuacao, $experiencia, $admissao, $demissao, $descricao)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	case 'exclusaoExperiencia' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST
		$id_exp = $_SESSION['id_reg'];
		if (deleteExperiencia($id_exp)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	case 'cadastroEmpresa' :
		//FAZ A INTERAÇÃO, CAPTURANDO OS DADOS VIA POST, CADASTRO DO CURRICULO
		$nomef = filter_input(INPUT_POST, 'nomef', FILTER_SANITIZE_STRING);
		$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
		$senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
		$rsocial = filter_input(INPUT_POST, 'rsocial', FILTER_SANITIZE_STRING);
		$cnpj = filter_input(INPUT_POST, 'cnpj', FILTER_SANITIZE_STRING);
		$celular = filter_input(INPUT_POST, 'celular', FILTER_SANITIZE_STRING);
		$telFixo = filter_input(INPUT_POST, 'telFixo', FILTER_SANITIZE_STRING);
		$cidade = filter_input(INPUT_POST, 'cidade', FILTER_SANITIZE_STRING);
		$bairro = filter_input(INPUT_POST, 'bairro', FILTER_SANITIZE_STRING);
		$endereco = filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING);
		$numero = filter_input(INPUT_POST, 'numero', FILTER_SANITIZE_STRING);
		$cep = filter_input(INPUT_POST, 'cep', FILTER_SANITIZE_STRING);
		

		if (addCadastroEmpresa($nomef, $email, $senha, $rsocial, $cnpj, $celular, $telFixo, $cidade, $bairro, $endereco, $numero,$cep)) :
			echo 'salvou';
		else :
			echo 'erro';
		endif;
		break;
	default :
		echo 'erro';
		break;
endswitch;
ob_end_flush();
