<?PHP
// +---------------------------------------------------------+
// | Emiss�o do boleto banc�rio                              |
// +---------------------------------------------------------+
// | Parte integrante do livro da s�rie Fa�a um Site         |
// | PHP 5 com banco de dados MySQL - Com�rcio eletr�nico    |
// | Editora �rica - autor: Carlos A J Oliviero              |
// | www.facaumsite.com.br                                   |
// +---------------------------------------------------------+
error_reporting(0);
include "Funcoes/conexao/conexao.php";
$pdo = conectar();
$consulta = $pdo->query("SELECT * FROM boleto where id_pessoafisica = (SELECT MAX(id_pessoafisica) FROM boleto);");
while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
    // aqui eu mostro os valores de minha consulta
	$id_pessoafisica = $linha['id_pessoafisica'];
    $nm_pessoa = $linha['nm_pessoa'];
	$ds_endereco = $linha['ds_endereco'];
	$ds_num_end = $linha['ds_num_end'];
	$nm_cep = $linha['nm_cep'];
	$ds_bairro = $linha['ds_bairro'];
	$nm_cidade = $linha['nm_cidade'];
	$ds_uf = $linha['ds_uf'];
	$vl_plano = $linha['vl_plano'];
	$dt_cad = $linha['dt_cad'];  
}

//$todasBoleto = $listaBoleto->fetchAll(PDO::FETCH_OBJ);
//$contaBoleto = $listaBoleto->rowCount();

// Calcula o valor do boleto
$_SESSION['valor_boleto'] = $vl_plano;
// Mant�m zeros na casa decimal sem formata��o para o c�lculo da linha digit�vel
$_SESSION['valor_boleto1'] = number_format($_SESSION['valor_boleto'],2,'','');
$_SESSION['num_ped'] = date('Y'). "-" . $id_pessoafisica. "." . date('m');

// ******** DADOS DO BOLETO
// DADOS FIXOS DE CONFIGURA��O DO BOLETO
// 1. Dados da sua empresa
$boleto["cedente_nome"] = "CASSO - CARIRI SOLU��ES EM SISTEMAS ONLINE LTDA";
$boleto["cedente_cnpj"] = "33.333.333/0001-33";

// 2. Dados da conta banc�ria da empresa (devem ser confirmados com o banco do cedente)
$boleto['num_banco'] = "935";				// Identifica��o do Banco (Banco teste = 935)
$boleto['dv_banco'] = "6";				// Identifica��o do Banco (Banco teste = 935)
$boleto['moeda'] = "9";							// C�digo da Moeda (Real = 9)
$boleto["num_agencia"] = "0572";		// Num da ag�ncia - sem digito verificador
$boleto["dv_agencia"] = "8";				// D�gito verificador da ag�ncia
$boleto["num_conta"] = "5543771";		// Num da conta corrente sem o d�gito verificador
$boleto["dv_conta"] = "8";					// Digito verificador da conta corrente

// 3. Dados restritos do banco (devem ser confirmados com o banco do cedente)
$boleto["carteira"] = "06";			// C�digo da Carteira: Consultar seu banco
$boleto["aceite"] = "N";				// Aceite: Consultar seu banco
$boleto["especie"] = "R$";			// Esp�cie: Consultar seu banco
$boleto["especie_doc"] = "99";	// Esp�cie documento: Consultar seu banco
$boleto["fixo"] = "0";					// posicao 44 do c�digo de barras

// 4. Informa��es gerais do boleto
$boleto['dv_codbar'] = "";	// D�gito verificador do C�digo de Barras

$boleto['fator'] = date('d/m/Y', strtotime('+15 days'));
$boleto['valor_zeroesq'] = zero_esquerda($_SESSION['valor_boleto1'],10);
$boleto['nosso_numero'] = zero_esquerda($id_pessoafisica,11);
$_SESSION['datavenc'] = date('d/m/Y', strtotime('+15 days'));
// Taxa de cobran�a para envio do boleto
$taxa_boleto = 3.00;

// Dados do Sacado
$boleto['sacado_nome'] = $nm_pessoa;
$boleto['sacado_end1'] = ltrim($ds_endereco) . ", " . ltrim($ds_num_end); 
$boleto['sacado_end2'] = substr($nm_cep,0,5)."-" . substr($nm_cep,5,3) . "&nbsp;&nbsp;&nbsp;&nbsp;" . ltrim($ds_bairro) . "&nbsp;&nbsp;&nbsp;&nbsp;" . ltrim($nm_cidade) . "&nbsp;&nbsp;&nbsp;&nbsp;" . ltrim($ds_uf);

// *************** C�DIGO DE BARRAS
// Monta c�gigo de barras sem o d�gito verificador
$boleto['codbarra_sem_dv'] = $boleto['num_banco'] . $boleto['moeda'] . $boleto['fator'] . $boleto['valor_zeroesq'] . $boleto['num_agencia'] . $boleto['carteira'] . $boleto['nosso_numero'] . $boleto['num_conta'] . $boleto['fixo'];

// C�lculo do d�gito verificador (dv) do c�digo de barras
// Inverte o c�digo de barras para c�lculo do d�gito verificador
$boleto1 = strrev($boleto['codbarra_sem_dv']);
$soma = 0;
for($i=0; $i <= 42; $i++) {
	$a[$i] = substr($boleto1,$i,1);
	// Multiplica as posi��es de 0 a 7 por 2,3,4,5,6,7,8 e 9
	if ($i <= 7) {
		$fator[$i] = $a[$i] * ($i + 2);
	}
	// Multiplica as posi��es de 8 a 15 por 2,3,4,5,6,7,8 e 9
	if ($i >= 8 and $i <= 15) {
		$fator[$i] = $a[$i] * ($i - 6);
	}
	// Multiplica as posi��es de 16 a 23 por 2,3,4,5,6,7,8 e 9
	if ($i >= 16 and $i <= 23) {
		$fator[$i] = $a[$i] * ($i - 14);
	}
	// Multiplica as posi��es de 24 a 31 por 2,3,4,5,6,7,8 e 9
	if ($i >= 24 and $i <= 31) {
		$fator[$i] = $a[$i] * ($i - 22);
	}
	// Multiplica as posi��es de 42 a 39 por 2,3,4,5,6,7,8 e 9
	if ($i >= 32 and $i <= 39) {
		$fator[$i] = $a[$i] * ($i - 30);
	}
	// Multiplica as posi��es de 40 a 42 por 2,3 e 4
	if ($i >= 40) {
		$fator[$i] = $a[$i] * ($i - 38);
	}
	// Soma os n�meros de cada posi��o do c�digo de barras invertido pelo respectivo fator
	$soma = $soma + $fator[$i];
	// Calcula o resto da divis�o entre a soma e 11
	$dv = 11 - ($soma % 11);
	// Se o resultado da subtra��o (11 - resto de $soma) for igual a 0 (Zero), 1 (um)
	// ou maior que 9 (nove) dever�o assumir o d�gito igual a 1 (um).
	if ($dv == 0 or $dv == 1 or $dv > 9) {
		$dv = 1;
	}
}
// Monta o c�digo de barras com o d�gito verificador (dv)
$boleto['codbarra_dv'] = substr($boleto['codbarra_sem_dv'],0,4) . $dv . substr($boleto['codbarra_sem_dv'],4,39);

// TRANSFORMA O C�DIGO DE BARRAS NUM�RICO (base 10) EM BIN�RIO
$n[0] = "00110";
$n[1] = "10001";
$n[2] = "01001";
$n[3] = "11000";
$n[4] = "00101";
$n[5] = "10100";
$n[6] = "01100";
$n[7] = "00011";
$n[8] = "10010";
$n[9] = "01010";
$boleto['codbarra_binario'] = "";
for($z=0; $z < 44; $z = $z+2) {
	for($i=0; $i < 5; $i++) {
		$x1 = substr($boleto['codbarra_dv'],$z,1);
		$x2 = substr($boleto['codbarra_dv'],$z+1,1);
		$boleto['codbarra_binario'] = $boleto['codbarra_binario'] . substr($n[$x1],$i,1) . substr($n[$x2],$i,1);
		
		
	}
	
}
// prepara as barras para impress�o
$boleto['cod_grafico'] = "";	// Cont�m as imagens das barras (preta e branca)
for($i=0; $i < strlen($boleto['codbarra_binario']); $i++) {
	// Verifica se a posi��o da barra � par
	if ($i % 2 == 0) {
		// Se o n�mero for zero imprime barra preta estreita
		if (substr($boleto['codbarra_binario'],$i,1) == 0) {
			$boleto['cod_grafico'] = $boleto['cod_grafico'] . "<img src='imagens/p.png' width='1' height='50' border='0' />";
		// Se o n�mero for 1 imprime barra preta larga
		} else {
			$boleto['cod_grafico'] = $boleto['cod_grafico'] . "<img src='imagens/p.png' width='3' height='50' border='0' />";
		}
	} 
	
	// Verifica se a posi��o da barra � �mpar
	if ($i % 2 == 1) {	
		// Se o n�mero for zero imprime barra branca estreita	
		if (substr($boleto['codbarra_binario'],$i,1) == 0) {
			$boleto['cod_grafico'] = $boleto['cod_grafico'] . "<img src='imagens/b.png' width='1' height='50' border='0' />";
		// Se o n�mero for 1 imprime barra branca larga		
		} else {
			$boleto['cod_grafico'] = $boleto['cod_grafico'] . "<img src='imagens/b.png' width='3' height='50' border='0' />";
		}
	}
}
// Insere ao c�digo de barras as barras start
$bar_start = "<img src='imagens/p.png' width='1' height='50' />";
$bar_start = $bar_start . "<img src='imagens/b.png' width='1' height='50' />";
$bar_start = $bar_start . "<img src='imagens/p.png' width='1' height='50' />";
$bar_start = $bar_start . "<img src='imagens/b.png' width='1' height='50' />";
$boleto['cod_grafico'] = $bar_start . $boleto['cod_grafico'];

// Insere ao c�digo de barras as barras stop
$bar_stop = "<img src='imagens/p.png' width='3' height='50' />";
$bar_stop = $bar_stop . "<img src='imagens/b.png' width='1' height='50' />";
$bar_stop = $bar_stop . "<img src='imagens/p.png' width='1' height='50' />";
$boleto['cod_grafico'] = $boleto['cod_grafico'] . $bar_stop;


// *************** LINHA DIGIT�VEL
// Campo livre = Ag�ncia (cedente) + Carteira + Nosso n�mero + Conta corrente (cedente) + Fixo (0)
$campo_livre = $boleto['num_agencia'] . $boleto["carteira"] . $boleto["nosso_numero"] . $boleto["num_conta"] . $boleto["fixo"];

// 1� campo
// Composto pelo c�digo de Banco, c�digo da moeda, as cinco primeiras posi��es do campo livre e o d�gito de auto confer�ncia(DAC) deste campo
$campo1 = $boleto['num_banco'] . $boleto['moeda'] . substr($campo_livre,0,5);
$dac_campo1 = calculo_dac1($campo1);

// 2� campo
// Composto pelas posi��es 6� a 15� do campo livre e o d�gito verificador deste campo
$campo2 = substr($campo_livre,5,10);
$dac_campo2 = calculo_dac2($campo2);

// 3� campo
// Composto pelas posi��es 16� a 25� do campo livre e o d�gito verificador deste campo deste campo
$campo3 = substr($campo_livre,15,10);
$dac_campo3 = calculo_dac2($campo3);

// 4� campo
// Composto pelo d�gito verificador do c�digo de barras, ou seja, a 5� posi��o do c�digo de barras
$campo4 = $dv;

// 5� campo
// Composto pelo fator de vencimento com 4(quatro) caracteres e o valor do documento com 10(dez) caracteres, sem separadores e sem edi��o
$campo5 = fator_venc(date('d-m-y') + (date('d')+15)) . zero_esquerda($_SESSION['valor_boleto1'],10);

// LINHA DIGIT�VEL
$linha_digitavel = substr($campo1,0,5) . "." . substr($campo1,5,5) . $dac_campo1 . " ";
$linha_digitavel = $linha_digitavel . substr($campo2,0,5) . "." . substr($campo2,5,5) . $dac_campo2 . " ";
$linha_digitavel = $linha_digitavel . substr($campo3,0,5) . "." . substr($campo3,5,5) . $dac_campo3 . " ";
$linha_digitavel = $linha_digitavel . $campo4 . " " . $campo5;

// *************** FUN��ES
// CALCULO DO FATOR DE VENCIMENTO DO BOLETO
// Par�metro: $data = Data de vencimento do boleto no formato aaaa-mm-dd
function fator_venc($data) {
	// Separa a data em dia, m�s e ano
	$dia = substr($data,8,2);
	$mes = substr($data,5,2);
	$ano = substr($data,0,4);
	// calcula o timestamp da data 07/10/1997 (base de c�lculo do fator de vencimento)
	$timestamp_data1 = mktime(0,0,0,10,07,1997);
	// calcula o timestamp da data de vencimento do boleto
	$timestamp_data2 = mktime(0,0,0,$mes,$dia,$ano);
	// Calcula a diferen�a de dias entre as duas datas. Como esta diferen�a � calculada em segundos, 
	// � necess�rio se dividir esse resultado por 86.400 (n�mero de segundos de 1 dia)
	$dif_dias = round(($timestamp_data2 - $timestamp_data1) / 86400);
return $dif_dias;
}

// INSERE ZEROS � ESQUERDA DE UM N�MERO
// Par�metros: $numero = n�mero considerado, $zeros = tamanho do n�mero (com zeros)
function zero_esquerda($numero,$zeros) {	
	// Retira o ponto decimal do n�mero	
	$numero = str_replace(".","",$numero);
	// Define o n�mero de zeros a serem inseridos � esquerda do n�mero
	$loop = $zeros - strlen($numero);				
	for($i=0; $i < $loop; $i++) {
		$numero = "0" . $numero;
	}
return $numero;
}

// Fun��o formatar CNPJ
function formatar_cnpj($n) {
$cnpj_formatado = substr($n,0,2).".".substr($n,2,3).".".substr($n,5,3)."/".substr($n,8,4)."-".substr($n,12,2);
return $cnpj_formatado;
}

// C�lculo do D�gito de auto confer�ncia (DAC) da linha digit�vel para o campo 1
function calculo_dac1($campo) {
$soma_dac = 0;
for($i=0; $i < 9; $i++) {
	// Varifica a posi��o do n�mero. Se impar $fator_dac = 2. Se par $fator_dac = 1
	if ($i % 2 == 0) {
		$fator_dac = 2;
	} else {
		$fator_dac = 1;
	}
	// Multiplica a posi��o do n�mero pelo $fator_dac
	$dac1 = (substr($campo,$i,1) * $fator_dac);
	// Se o valor de $dac1 for maior do que 9, somam-se os dois d�gitos, ex:
	// Se $dac1 = 12 teremos como resultado final 1 + 2, ou seja 3.
	if ($dac1 > 9) {
		$dac2 = substr($dac1,0,1) + substr($dac1,1,1);
	} else {
		$dac2 = $dac1;
		
	}
	$soma_dac = $dac2 + $soma_dac;
	
	// Divide-se o resultado por 10, se resto = 0 o DAC ser� 0
	// Se resto diferente de 0 o DAC ser�: 10 - resto
	if ($soma_dac % 10 == 0) {
		$dac = 0;
	} else {
		$dac = 10 - ($soma_dac % 10);
	}
}
return $dac;
}	

// C�lculo do D�gito de auto confer�ncia (DAC) da linha digit�vel para o campo 2 e 3
function calculo_dac2($campo) {
$soma_dac = 0;
for($i=0; $i < 10; $i++) {
	// Varifica a posi��o do n�mero. Se impar $fator_dac = 2. Se par $fator_dac = 1
	if ($i % 2 == 0) {
		$fator_dac = 1;
	} else {
		$fator_dac = 2;
	}
	// Multiplica a posi��o do n�mero pelo $fator_dac
	$dac1 = (substr($campo,$i,1) * $fator_dac);
	// Se o valor de $dac1 for maior do que 9, somam-se os dois d�gitos, ex:
	// Se $dac1 = 12 teremos como resultado final 1 + 2, ou seja 3.
	if ($dac1 > 9) {
		$dac2 = substr($dac1,0,1) + substr($dac1,1,1);
	} else {
		$dac2 = $dac1;
	}
	$soma_dac = $soma_dac + $dac2;
	// Divide-se o resultado por 10, se resto = 0 o DAC ser� 0
	// Se resto diferente de 0 o DAC ser�: 10 - resto
	if ($soma_dac % 10 == 0) {
		$dac = 0;
	} else {
		$dac = 10 - ($soma_dac % 10);
	}
}
return $dac;
}	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Emprega Cariri - Boleto Banc�rio</title>
<style type="text/css">

.linha_inf {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
	font-weight: bold;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 2px;
	padding-left: 10px;
}
.linha_dir {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #000000;
	font-weight: bold;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 2px;
	padding-left: 10px;
}
.titulo_inf {
	font-size: 8px;
	line-height: 10px;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 0px;
	padding-left: 2px;
}
.titulo_dir {
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #000000;
	font-size: 8px;
	line-height: 10px;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 0px;
	padding-left: 2px;
}
.logo_banco {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #000000;
	font-weight: bold;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 2px;
	padding-left: 0px;
}
.logo_fs {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
	font-weight: bold;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 2px;
	padding-left: 0px;
}
.num_banco {
	padding: 2px;
	font-size: 18px;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #000000;
	text-align: center;
}
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.linha_digitavel {
	padding: 2px;
	font-size: 16px;
	font-weight: normal;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
	text-align: right;
}
.valor {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
	font-weight: bold;
	padding-top: 0px;
	padding-right: 15px;
	padding-bottom: 2px;
	padding-left: 10px;
	text-align: right;
}
.sacado {
	font-weight: bold;
	padding-top: 3px;
	padding-right: 15px;
	padding-bottom: 3px;
	padding-left: 30px;
}
.avalista {
	font-size: 8px;
	line-height: 10px;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 0px;
	padding-left: 2px;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}
.autenticacao {
	font-size: 9px;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 0px;
	padding-left: 2px;
}
.instrucoes {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
	padding-top: 0px;
	padding-right: 2px;
	padding-bottom: 2px;
	padding-left: 10px;
}
p {
	padding-top: 1px;
	padding-right: 0px;
	padding-bottom: 1px;
	padding-left: 0px;
	margin: 0px;
}
.linha_digitavelA {
	padding: 2px;
	font-size: 20px;
	font-weight: normal;
}
.titulo {
	padding: 2px;
	font-size: 14px;
	font-weight: normal;
	text-align: right;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}

</style>
</head>
<body onload="javascript:window.print();">
<!-- Menu de emiss�o do boleto -->
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
	<td height="28" valign="top" background="imagens/menu_boleto.gif"><div align="right">
		<a href="javascript:window.print();"><img src="imagens/btn_transparente.gif" alt="Imprimir"width="85" height="23" hspace="3" border="0" /></a>
		<a href="index.php"><img src="imagens/btn_transparente.gif" alt="Encerrar se&ccedil;&atilde;o" width="71" height="23" hspace="3" border="0" /></a>		
	</div></td>
</tr>
</table>

<!-- Recibo do Sacado -->
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="logo_fs"><a href="index.php"><img src="imagens/logo_boleto.png" width="178" height="37" vspace="2" border="0" /></a></td>
    <td width="439" class="titulo">Boleto para pagamento do pedido n�<strong>&nbsp;<?PHP print $_SESSION['num_ped']; ?></strong></td>
  </tr>
</table>

<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="5" class="titulo_dir">Local de Pagamento</td>
    <td width="146" class="titulo_inf">Vencimento</td>
  </tr>
  <tr>
    <td colspan="5" class="linha_dir">PAGAVEL PREFERENCIALMENTE EM QUALQUER AG�NCIA BANCO TESTE</td>
    <td class="linha_inf"><?PHP print $_SESSION['datavenc']; ?></td>
  </tr>
  <tr>
    <td colspan="5" class="titulo_dir">Cedente</td>
    <td class="titulo_inf">Ag�ncia/C�digo Cedente</td>
  </tr>
  <tr>
    <td colspan="5" class="linha_dir"><?PHP print strtoupper($boleto["cedente_nome"]); ?></td>
    <td class="linha_inf"><?PHP print $boleto["num_agencia"] . "-" . $boleto["dv_agencia"] . "/" . $boleto["num_conta"] . "-" . $boleto["dv_conta"]; ?></td>
  </tr>
  <tr>
    <td width="109" class="titulo_dir">Data do Documento </td>
    <td width="129" class="titulo_dir">N&ordm; do Documento </td>
    <td width="113" class="titulo_dir">Esp&eacute;cie Documento </td>
    <td width="84" class="titulo_dir">Aceite</td>
    <td width="119" class="titulo_dir">Data do Processamento</td>
    <td class="titulo_inf">Nosso N�mero</td>
  </tr>
  <tr>
    <td class="linha_dir"><?PHP print print date('d/m/Y'); ?></td>
    <td class="linha_dir"><?PHP print $_SESSION['num_ped']; ?></td>
    <td class="linha_dir"><?PHP print $boleto["especie_doc"]; ?></td>
    <td class="linha_dir"><?PHP print $boleto["aceite"]; ?></td>
    <td class="linha_dir"><?PHP print date('d/m/Y'); ?></td>
    <td class="linha_inf"><?PHP print $boleto['carteira'] . "-" . $boleto['nosso_numero']; ?></td>
  </tr>
  <tr>
    <td class="titulo_dir">Uso do Banco </td>
    <td class="titulo_dir">Carteira</td>
    <td class="titulo_dir">Esp&eacute;cie</td>
    <td class="titulo_dir">Quantidade</td>
    <td class="titulo_dir">Valor</td>
    <td class="titulo_inf">(=) Valor do Documento </td>
  </tr>
  <tr>
    <td class="linha_dir">&nbsp;</td>
    <td class="linha_dir"><?PHP print $boleto['carteira']; ?></td>
    <td class="linha_dir"><?PHP print $boleto['especie']; ?></td>
    <td class="linha_dir">&nbsp;</td>
    <td class="linha_dir">&nbsp;</td>
    <td class="valor"><?PHP print number_format($_SESSION['valor_boleto'],2,',','.'); ?></td>
  </tr>
  <tr>
    <td colspan="5" class="titulo_inf">&nbsp;</td>
    <td class="titulo_inf">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" valign="top" class="instrucoes">
		<p><strong>Instru��es de impress�o</strong></p>
		<p>- Imprima em impressora jato de tinta (ink jet) ou laser em qualidade normal ou alta (N�o use modo econ�mico).</p>
		<p>- Utilize folha A4 (210 x 297 mm) ou Carta (216 x 279 mm) e margens m�nimas � esquerda e � direita do formul�rio.</p>
		<p>- Corte na linha indicada. N�o rasure, risque, fure ou dobre a regi�o onde se encontra o c�digo de barras.</p>
		<p>&nbsp;</p>
		<p><strong>Pagamento via Internet Banking</strong></p>
		<p>Caso tenha problemas ao imprimir este boleto, ou se desejar pag�-lo atrav�s do Internet Banking, utilize a linha digit�vel descrita abaixo:</p>
		<p>&nbsp;</p>
		<p align="center" class="linha_digitavelA"><?PHP print $linha_digitavel; ?></p>
		<p>&nbsp;</p>	
		</td>
  </tr>
  
  <tr>
    <td colspan="6" class="titulo_inf">Sacado</td>
  </tr>
  <tr>
    <td colspan="6" class="sacado">
		<?PHP print $boleto['sacado_nome']; ?><br />
		<?PHP print $boleto['sacado_end1']; ?><br />
		<?PHP print $boleto['sacado_end2']; ?></td>
  </tr>
  <tr>
    <td colspan="6" class="avalista">Sacador/Avalista</td>
  </tr>
  <tr>
    <td colspan="6"><div align="right"><strong>Recibo do Sacado  -</strong> <span class="autenticacao">Autentica&ccedil;&atilde;o Mec&acirc;nica</span> </div></td>
  </tr>
	
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>		
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>	
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>	
  <tr>
    <td colspan="6"><img src="imagens/corte.gif" width="700" height="12" /></td>
  </tr>	
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>	
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>						
</table>

<!-- Ficha de Compensa��o -->
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="157" class="logo_banco"><img src="imagens/logo_banco.gif" width="140" height="23" /></td>
    <td width="51" class="num_banco"><?PHP print $boleto['num_banco'] . "-" . $boleto['dv_banco']; ?></td>
    <td width="492" class="linha_digitavel"><?PHP print $linha_digitavel; ?></td>
  </tr>
</table>

<!-- Ficha de compensa��o -->
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="5" class="titulo_dir">Local de Pagamento</td>
    <td width="146" class="titulo_inf">Vencimento</td>
  </tr>
  <tr>
    <td colspan="5" class="linha_dir">PAGAVEL PREFERENCIALMENTE EM QUALQUER AG�NCIA BANCO TESTE</td>
    <td class="linha_inf"><?PHP print $_SESSION['datavenc']; ?></td>
  </tr>
  <tr>
    <td colspan="5" class="titulo_dir">Cedente</td>
    <td class="titulo_inf">Ag�ncia/C�digo Cedente</td>
  </tr>
  <tr>
    <td colspan="5" class="linha_dir"><?PHP print strtoupper($boleto["cedente_nome"]); ?></td>
    <td class="linha_inf"><?PHP print $boleto["num_agencia"] . "-" . $boleto["dv_agencia"] . "/" . $boleto["num_conta"] . "-" . $boleto["dv_conta"]; ?></td>
  </tr>
  <tr>
    <td width="109" class="titulo_dir">Data do Documento </td>
    <td width="129" class="titulo_dir">N&ordm; do Documento </td>
    <td width="113" class="titulo_dir">Esp&eacute;cie Documento </td>
    <td width="84" class="titulo_dir">Aceite</td>
    <td width="119" class="titulo_dir">Data do Processamento</td>
    <td class="titulo_inf">Nosso N�mero</td>
  </tr>
  <tr>
    <td class="linha_dir"><?PHP print date('d/m/Y'); ?></td>
    <td class="linha_dir"><?PHP print $_SESSION['num_ped']; ?></td>
    <td class="linha_dir"><?PHP print $boleto["especie_doc"]; ?></td>
    <td class="linha_dir"><?PHP print $boleto["aceite"]; ?></td>
    <td class="linha_dir"><?PHP print date('d/m/Y'); ?></td>
    <td class="linha_inf"><?PHP print $boleto['carteira'] . "-" . $boleto['nosso_numero']; ?></td>
  </tr>
  <tr>
    <td class="titulo_dir">Uso do Banco </td>
    <td class="titulo_dir">Carteira</td>
    <td class="titulo_dir">Esp&eacute;cie</td>
    <td class="titulo_dir">Quantidade</td>
    <td class="titulo_dir">Valor</td>
    <td class="titulo_inf">(=) Valor do Documento </td>
  </tr>
  <tr>
    <td class="linha_dir">&nbsp;</td>
    <td class="linha_dir"><?PHP print $boleto['carteira']; ?></td>
    <td class="linha_dir"><?PHP print $boleto['especie']; ?></td>
    <td class="linha_dir">&nbsp;</td>
    <td class="linha_dir">&nbsp;</td>
    <td class="valor"><?PHP print number_format($_SESSION['valor_boleto'],2,',','.'); ?></td>
  </tr>
  <tr>
    <td colspan="5" class="titulo_dir">INSTRU&Ccedil;&Otilde;ES (Texto de responsabilidade do Cedente) </td>
    <td class="titulo_inf">(-) Desconto/Abatimento </td>
  </tr>
  <tr>
    <td colspan="5" rowspan="9" valign="top" class="linha_dir">
		<p>&nbsp;</p>
		<p>ATEN��O:</p>
		<p>- N�o pague este boleto ap�s o seu vencimento.</p>
		<p>- Ap�s esta data o pedido ser� cancelado e o boleto perder� a validade.
		<p>&nbsp;</p>
		<p>BOLETO PARA FINS DID�TICOS&nbsp;&nbsp;&nbsp;*** NUNCA EFETUE SEU PAGAMENTO ***</p>
		</td>
    <td class="linha_inf">&nbsp;</td>
  </tr>
  <tr>
    <td class="titulo_inf">(-) Outras Dedu&ccedil;&otilde;es </td>
  </tr>
  <tr>
    <td class="linha_inf">&nbsp;</td>
  </tr>
  <tr>
    <td class="titulo_inf">(+) Mora/Multa </td>
  </tr>
  <tr>
    <td class="linha_inf">&nbsp;</td>
  </tr>
  <tr>
    <td class="titulo_inf">(+) Outros Acr&eacute;scimos </td>
  </tr>
  <tr>
    <td class="linha_inf">&nbsp;</td>
  </tr>
  <tr>
    <td class="titulo_inf">(=) Valor Cobrado </td>
  </tr>
  <tr>
    <td class="linha_inf">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" class="titulo_inf">Sacado</td>
  </tr>
  <tr>
    <td colspan="6" class="sacado">
		<?PHP print $boleto['sacado_nome']; ?><br />
		<?PHP print $boleto['sacado_end1']; ?><br />
		<?PHP print $boleto['sacado_end2']; ?></td>
  </tr>
  <tr>
    <td colspan="5" class="avalista">Sacador/Avalista</td>
    <td class="avalista"><div align="right">C&oacute;digo de Baixa </div></td>
  </tr>
  <tr>
    <td colspan="6"><div align="right"><strong>Ficha de Compensa&ccedil;&atilde;o -</strong> <span class="autenticacao">Autentica&ccedil;&atilde;o Mec&acirc;nica</span> </div></td>
  </tr>
	
  <tr>
    <td colspan="6"><?PHP print $boleto['cod_grafico']; ?></td>
  </tr>	
  
</table>
</body>
</html>
