<?php 
session_start();
$nome = isset($_SESSION['NM_CLIENTE'])? $_SESSION['NM_CLIENTE'] : "Usuário";
include 'funcoes/conexao/conexao.php';
$pdo = conectar ();
include 'funcoes/crud/crud.php';
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Empresa</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="css/formEmpresa.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>

<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<!-- icones do topo -->
			<?php
			include_once 'includes/icones-topo.php';
			?>

			<!-- include do menu -->
			<?php
			include_once 'includes/menu.php';
			?>
		</nav>
		<!-- fim da navegação -->

		<div id="page-wrapper">
			<br />
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-success">
						<div class="panel-heading">
							<span
								style="color: #303030; font-weight: bold; font-size: 15px; text-transform: uppercase;">
								<center>Vagas em destaques</center>
							</span>
						</div>
						<div class="panel-body">
							<div class="row">
								<div id="retorno"></div>
								<div class="col-lg-12">
									<ul class="nav" id="buscador">
										<li class="sidebar-search">
											<form action="results.php" role="form" name="formBuscaCargo" id="formBuscaCargo" method="post">
												<div class="input-group custom-search-form">
													<input type="text" name="buscaCargo" id="buscaCargo" class="form-control" placeholder="Busque um cargo" />
													<span class="input-group-btn">
														<button class="btn btn-default" type="submit" name="btnBuscaCargo" id="btnBuscaCargo">
															<i class="fa fa-search"></i>
														</button>
													</span>
												</div>
											</form>
										</li>
									</ul>
									<!-- Fim do campo buscar -->
									<div class="table-responsive" id="resultado">
										<table class="table table-bordered" id="tabelaCargo">
											<thead>
												<tr>
													<th><span>Cargos</span></th>
												</tr>
											</thead>
											
											<!-- pegando os cargos via controller -->
											<tbody id="linhaTabela">
				                            	<?php
					                            	if (listarCargos ()) :
													$cargo = listarCargos ();
													foreach ( $cargo as $listar ) :
												?>
													<tr>
														<td><?php echo $listar['NM_CARGO']; ?></td>
													</tr>
												<?php
														endforeach;
													endif;
												?>
											</tbody>
										</table>
									</div>
								</div>
								<!-- col-lg-6 (nested) -->
							</div>
							<!-- row (nested) -->
						</div>
						<!-- panel-body -->
					</div>
					<!-- panel -->
				</div>
				<!-- col-lg-12 -->
			</div>
			<!-- row -->
		</div>
		<!-- fim div formulários -->
	</div>
	<!-- fim div conteúdo mae -->

	<!-- jQuery Version 1.11.0 -->
	<script src="js/jquery-1.11.0.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Metis Menu Plugin JavaScript -->
	<script src="js/plugins/metisMenu/metisMenu.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
	<script src="js/formEmpresaAnuncio.js" type="text/javascript"></script>
</body>
</html>