<?php

//FUNÇÃO DE CADASTRO DO FORMULÁRIO DO CANDIDATO
function addCadastroUsuario($nome, $sobrenome, $email, $senha) {
    $pdo = conectar();

    try {
        $cadastro = $pdo->prepare("INSERT INTO usuario (nm_nome, nm_sobrenome, ds_email, ds_senha) VALUES (?,?,?,?)") or die(mysql_erro());
        $cadastro->bindValue(1, $nome, PDO::PARAM_STR);
        $cadastro->bindValue(2, $sobrenome, PDO::PARAM_STR);
        $cadastro->bindValue(3, $email, PDO::PARAM_STR);
        $cadastro->bindValue(4, $senha, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}

//FUNÇÃO DE CADASTRO DO FORMULÁRIO DE CURRÍCULO DO CANDIDATO
function addCadastroCandidato($nome, $email, $telefone1, $telefone2, $endereco, $numero, $bairro, $cidade1, $cep, $senha, $sexo, $escolaridade, $cpf, $rg, $dtNasc, $plano, $dt_cad) {
    $pdo = conectar();
    try {
        $cadastro = $pdo->prepare("CALL spi_pessoa_fisica_comp(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)") or die(mysql_erro());
        $cadastro->bindValue(1, $nome, PDO::PARAM_STR);
        $cadastro->bindValue(2, $email, PDO::PARAM_STR);
        $cadastro->bindValue(3, $telefone1, PDO::PARAM_STR);
        $cadastro->bindValue(4, $telefone2, PDO::PARAM_STR);
        $cadastro->bindValue(5, $endereco, PDO::PARAM_STR);
        $cadastro->bindValue(6, $numero, PDO::PARAM_STR);
        $cadastro->bindValue(7, $bairro, PDO::PARAM_STR);
        $cadastro->bindValue(8, $cidade1, PDO::PARAM_INT);
        $cadastro->bindValue(9, $cep, PDO::PARAM_STR);
        $cadastro->bindValue(10, $senha, PDO::PARAM_STR);
        $cadastro->bindValue(11, $sexo, PDO::PARAM_STR);
        $cadastro->bindValue(12, $escolaridade, PDO::PARAM_STR);
        $cadastro->bindValue(13, $cpf, PDO::PARAM_STR);
        $cadastro->bindValue(14, $rg, PDO::PARAM_STR);
        $cadastro->bindValue(15, $dtNasc, PDO::PARAM_STR);
        $cadastro->bindValue(16, $plano, PDO::PARAM_INT);
        $cadastro->bindValue(17, $dt_cad, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}

function addCadastroEmpresa($nomeEmpresa, $emailEmpresa, $telefoneEmpresa1, $telefoneEmpresa2, $enderecoEmpresa, $numeroEmpresa, $bairroEmpresa, $cidadeEmpresa, $cepEmpresa, $senhaEmpresa, $fantasiaEmpresa, $siteEmpresa, $cnpjEmpresa, $ieEmpresa, $faxEmpresa, $dt_cadEmpresa) {
    $pdo = conectar();

    try {
        $cadastro = $pdo->prepare("CALL spi_pessoa_juridica_comp(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $nomeEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(2, $emailEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(3, $telefoneEmpresa1, PDO::PARAM_STR);
        $cadastro->bindValue(4, $telefoneEmpresa2, PDO::PARAM_STR);
        $cadastro->bindValue(5, $enderecoEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(6, $numeroEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(7, $bairroEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(8, $cidadeEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(9, $cepEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(10, $senhaEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(11, $fantasiaEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(12, $siteEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(13, $cnpjEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(14, $ieEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(15, $faxEmpresa, PDO::PARAM_STR);
        $cadastro->bindValue(16, $dt_cadEmpresa, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}

function addCVCapacitacao($idPessoaCapacitacao, $capacitacaoCapacitacao, $entidadeCapacitacao, $cargaHorariaCapacitacao) {
    $pdo = conectar();

    try {
        $cadastro = $pdo->prepare("CALL spi_capacitacao(?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $idPessoaCapacitacao, PDO::PARAM_STR);
        $cadastro->bindValue(2, $capacitacaoCapacitacao, PDO::PARAM_STR);
        $cadastro->bindValue(3, $entidadeCapacitacao, PDO::PARAM_STR);
        $cadastro->bindValue(4, $cargaHorariaCapacitacao, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}

function addCVExperiencia($idPessoaExperiencia, $cargoExperiencia, $empresaExperiencia, $periodoExperiencia) {
    $pdo = conectar();

    try {
        $cadastro = $pdo->prepare("CALL spi_experiencia(?,?,?,?)") or die(mysql_error());
        $cadastro->bindValue(1, $idPessoaExperiencia, PDO::PARAM_STR);
        $cadastro->bindValue(2, $cargoExperiencia, PDO::PARAM_STR);
        $cadastro->bindValue(3, $empresaExperiencia, PDO::PARAM_STR);
        $cadastro->bindValue(4, $periodoExperiencia, PDO::PARAM_STR);
        $cadastro->execute();

        if ($cadastro->rowCount() == 1):
            return TRUE;
        else:
            return FALSE;
        endif;
    } catch (PDOExcetion $erro) {
        echo $erro->getMessage();
    }
}

//função de listagem de cargos
function listarCargos(){
	$pdo = conectar();
	
	try {
		$listarTodos = $pdo->query("SELECT * FROM cargo");
		if ($listarTodos -> rowCount() > 0):
			return $listarTodos->fetchAll(PDO::FETCH_ASSOC);
			
		else:
			return false;
		endif;
	} catch (PDOException $erro) {
		echo $erro->getMessage();
	}
}
?>
