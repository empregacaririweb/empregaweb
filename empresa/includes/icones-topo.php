<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" href="index.php"><?php echo $nome;?> - Painel de Controle</a>
</div><!-- Fim da div header da busca -->
<!-- Fim da div header da busca -->
<ul class="nav navbar-top-links navbar-right">
	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"
		href="#" style="color: green;"> <i
			class="glyphicon glyphicon-envelope"></i> <i class="fa fa-caret-down"></i>
	</a>
		<ul class="dropdown-menu dropdown-messages">
			<li><a href="#">
					<div>
						<strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em>
						</span>
					</div>
					<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Pellentesque eleifend...</div>
			</a></li>
			<!-- Div das mensagens -->

			<li class="divider"></li>

			<li><a href="#">
					<div>
						<strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em>
						</span>
					</div>

					<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Pellentesque eleifend...</div>
			</a></li>
			<!-- Div das mensagens -->

			<li class="divider"></li>

			<li><a href="#">
					<div>
						<strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em>
						</span>
					</div>
					<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Pellentesque eleifend...</div>
			</a></li>
			<!-- Div das mensagens -->

			<li class="divider"></li>

			<li><a class="text-center" href="#"> <strong>Todas as mensagens</strong>
					<i class="fa fa-angle-right"></i>
			</a></li>
			<!-- Div ver todas as mensagens -->
		</ul>
		<!-- Fim da div mensagens --></li>
	<!-- Fim do dropdown -->
</ul>
<!-- Fim navbar-top-links -->