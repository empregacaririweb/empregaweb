<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li>
				<a href="dadosCadastrais.php" style="color: green;"><i class="fa fa-pencil-square-o"></i> Dados Cadastrais</a>
			</li><!-- Fim da div dados -->

			<li>
				<a href="anuncioVaga.php" style="color: green;"><i class="fa fa-list-alt"></i> Anuncie suas vagas</a>
			</li><!-- Fim das vagas emprego -->

			<li>
				<a href="#" style="color: green;"><i class="fa fa-child fa-fw"></i> Recrutamento</a>
			</li><!-- Fim do recrutamento -->
			
			<li>
				<a href="../destroy.php" style="color: green;"><i class="fa fa-sign-out fa-fw"></i> Sair do sistema</a>
			</li>
		</ul>
	</div>
</div><!-- fim navbar -->