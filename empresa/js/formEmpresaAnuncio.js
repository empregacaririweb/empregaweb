$(document).ready(function(){
	$('form[name=formEmpresa]').submit(function(){
		
		var forma = $(this);
		var botao = $(this).find(':button');
		
		$.ajax({
			url: "controlador/ajax/controller.php",
			type: "POST",
			data: "acao=cadastrarFormVaga&" + forma.serialize(),
			beforeSend: function(){
				botao.html('Aguarde...').attr('disabled', true);
			},
			success: function(retorno){
				botao.attr('disabled', false).html('<i class="glyphicon glyphicon-ok"></i> Cadastrar');
				console.log(retorno);
			}
		});
		
		return false;
	});
	
	function listarCargos(url, acao){
		$.post(url, {acao: acao}, function(retorno){
			var tbody = $('.table').find('tbody');
			tbody.empty().html(retorno);
		});
	}
	
	listarCargos('controlador/ajax/controller.php', 'listar_cargo');
	
	//função de mensagem
	function msg(msg, tipo) {
        var retorno = $("#retorno");
        var tipo = (tipo === 'sucesso') ? 'success' : (tipo === 'alerta') ? 'warning' : (tipo === 'erro') ? 'danger' : (tipo === 'info') ? 'info' : alert('INFORME MENSAGEM DE SUCESSO, ALERTA, ERRO E INFO');

        retorno.empty().slideUp('fast', function () {
            return $(this).html('<div class="alert alert-' + tipo + '" role="alert">' + msg + '</div>').slideDown('slow');
        });
        setTimeout(function () {
            retorno.slideUp('slow');
        }, 5000);
    }
});
