<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<span style="color: #303030; font-weight: bold; font-size: 15px; text-transform: uppercase;">
									<center>
										Informe seus dados
									</center>
								</span>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<form action="" method="post" id="formEmpresa" name="formEmpresa" role="form" enctype="multipart/form-data">
											<div class="col-xs-8">
												<label for="nomeFantasia">* Nome Fantasia:</label>
												<input type="text" class="form-control" name="nomeFantasia" id="nomeFantasia" autofocus="autofocus" required="required" />
											</div>

											<div class="col-xs-4">
												<label for="cnpj">* CNPJ da empresa:</label>
												<input type="text" name="cnpj" id="cnpj" class="form-control" required="required" />
											</div>

											<div class="col-xs-5">
												<label for="nomeRazaoSocial">* Razão Social:</label>
												<input type="text" class="form-control" name="nomeRazaoSocial" id="nomeRazaoSocial" required="required" />
											</div>

											<div class="col-xs-5">
												<label for="enderecoEmpresa">* Endereço:</label>
												<input type="text" name="enderecoEmpresa" id="enderecoEmpresa" class="form-control" required="required" />
											</div>

											<div class="col-xs-2">
												<label for="numeroCasa">* Número:</label>
												<input type="number" name="numeroCasa" id="numeroCasa" class="form-control" required="required" />
											</div>

											<div class="col-xs-2">
												<label for="cepEmpresa">* CEP:</label>
												<input type="text" name="cepEmpresa" id="cepEmpresa" class="form-control" required="required" />
											</div>

											<div class="col-xs-5">
												<label for="celularEmpresa">* Celular:</label>
												<input type="tel" name="celularEmpresa" id="celularEmpresa" class="form-control" required="required" />
											</div>

											<div class="col-xs-5">
												<label for="telefoneEmpresa">* Telefone Fixo:</label>
												<input type="tel" name="telefoneEmpresa" id="telefoneEmpresa" class="form-control" required="required" />
											</div>
											
											<button type="submit" name="cadastrarDados" id="cadastrarDados" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Cadastrar</button>
										</form>
									</div><!-- col-lg-6 (nested) -->
								</div><!-- row (nested) -->
							</div><!-- panel-body -->
						</div><!-- panel -->
					</div><!-- col-lg-12 -->
				</div><!-- row -->