<?php
include_once '../funcoes/conexao/conexao.php';
$pdo = conectar ();
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>SB Admin 2 - Bootstrap Admin Theme</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="css/plugins/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="css/formEmpresa.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>
	<section id="sessaoMae">
		<div id="wrapper">
			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
				<!-- icones do topo -->
				<?php
				include_once '../includes/icones-topo.php';
				?>

				<!-- include do menu -->
				<?php
				include_once '../includes/menu.php';
				?>
			</nav>
			<!-- fim da navegação -->

			<div id="page-wrapper">
				<br />
				<div class="row">
					<div class="col-lg-12">
					
					</div>
				</div>
			</div>
			<!-- fim div formulários -->
		</div>
		<!-- fim div conteúdo mae -->
	</section>

	<!-- <footer>
			<div class="container" id="rodapeEmpresa">
                <div class="row">
                    <div class="span6">
                        <p>Copyright &copy; 2015, Emprega Cariri - Todos os Direitos Reservados </p>
                    </div>
                    <div class="span6">
                        <ul class="inline pull-right">
                            <li id="texto">Acompanhe nas redes sociais</li>
                            <li id="facebook"><a href="#"><img src="imagens/icon-facebook.png" alt="facebook icon"></a></li>
                            <li id="twitter"><a href="#"><img src="imagens/icon-twitter.png" alt="twitter icon"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
		</footer> -->

	<!-- jQuery Version 1.11.0 -->
	<script src="js/jquery-1.11.0.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Metis Menu Plugin JavaScript -->
	<script src="js/plugins/metisMenu/metisMenu.min.js"></script>
	<!-- Morris Charts JavaScript -->
	<script src="js/plugins/morris/raphael.min.js"></script>
	<script src="js/plugins/morris/morris.min.js"></script>
	<script src="js/plugins/morris/morris-data.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
</body>
</html>
