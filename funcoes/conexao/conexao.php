<?php

//FUNÇOES DE CONEXAO
define('HOST', 'localhost');
define('USUARIO', 'root');
define('SENHA', '');
define('DB', 'emprega_cariri');

// AQUI É A CONEXÃO QUE VC CHAMARA NAS PAGINAS CONECTAR
function conectar() {

	$dns = "mysql:host=" . HOST . ";dbname=" . DB;
	try {
		$conn = new PDO($dns, USUARIO, SENHA);
		$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $conn;
	} catch (PDOException $erro) {
		echo $erro -> getMessage();
	}
}
?>