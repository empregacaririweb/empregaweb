$(document).ready(function () {

    $('form[name="formCandidato"]').submit(function () {
        var form = $(this);
        var botao = form.find(':button');

        $.ajax({
            url: "controlador/ajax/controller.php",
            type: "POST",
            data: "acao=cadastroPessoais&" + form.serialize(),
            beforeSend: function () {
                botao.attr('disabled', true);
                $("#loads").fadeIn('slow');
            },
            success: function (retorno) {
                botao.attr('disabled', false);
                $("#loads").fadeOut('slow');
                if (retorno === 'salvou') {
                    msg('<center><strong>Cadastrado com sucesso!</strong></center>', 'sucesso');
                    $("#cpf").val("");
                    $("#dtNasc01").val("");
                    $("#dtNasc02").val("");
                    $("#dtNasc03").val("");
                    $("#estCivil").val("");
                    $("#sexo").val("");
                    $("#emp").val("");
					$("#cidade").val("");
                    $("#bairro").val("");
                    $("#endereco").val("");
                    $("#numero").val("");
                    $("#complemento").val("");
                    $("#cep").val("");
                    $("#celular").val("");
					$("#telFixo").val("");
                    $("#nome").val("");
                    $("#email").val("");
                    $("#senha").val("");
                } else {
                    msg('<center><strong>Ops! Um erro ocorreu!</strong></center>', 'erro');
					alert(retorno);
                }
            }
        });
        return false;
    });

    // FUNÇÕES GERAIS
    function msg(msg, tipo) {
        var retorno = $("#retorno");
        var tipo = (tipo === 'sucesso') ? 'info' : (tipo === 'info') ? 'info' : (tipo === 'erro') ? 'danger' : (tipo === 'alerta') ? 'warning' : alert('INFORME MENSAGEM DE SUCESSO, ALERTA, ERRO E INFO');

        retorno.empty().slideUp('fast', function () {
            return $(this).html('<div class="alert alert-' + tipo + '" role="alert">' + msg + '</div>').slideDown('slow');
        });
        setTimeout(function () {
            retorno.slideUp('slow');
        }, 4000);
    }
    $('#cpf').mask("999.999.999-99");
    $('#celular').mask("(99) 9 9999-9999");
    $('#cep').mask("99.999-999");
    $('#telFixo').mask("9999-9999");
});