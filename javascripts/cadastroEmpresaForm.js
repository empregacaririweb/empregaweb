$(document).ready(function () {

    $('form[name="formEmpresa"]').submit(function () {
        var form = $(this);
        var botao = form.find(':button');

        $.ajax({
            url: "../Controller/controller.php",
            type: "POST",
            data: "acao=cadastroUsuarioEmpresa&" + form.serialize(),
            beforeSend: function () {
                botao.attr('disabled', true);
                $("#loads").fadeIn('slow');
            },
            success: function (retorno) {
                botao.attr('disabled', false);
                $("#loads").fadeOut('slow');
                if (retorno === 'salvou') {
                    msg('<center><strong>Dados salvos com sucesso!</strong></center>', 'sucesso');
					 
                    $("#nomeEmpresa").val("");
                    $("#emailEmpresa").val("");
                    $("#telefoneEmpresa1").val("");
                    $("#telefoneEmpresa2").val("");
                    $("#enderecoEmpresa").val("");
                    $("#numeroEmpresa").val("");
                    $("#bairroEmpresa").val("");
                    $(".cidadeEmpresa").val("");
                    $("#cepEmpresa").val("");
					$("#senhaEmpresa").val("");
					$("#fantasiaEmpresa").val("");
					$("#siteEmpresa").val("");
					$("#cnpjEmpresa").val("");
					$("#ieEmpresa").val("");
					$("#faxEmpresa").val("");
					$("#dt_cadEmpresa").val("");
                    window.setTimeout(function () {
                        $(location).attr('href', 'cadastro_curriculo_pag2.php');
                    }, 4000);
                } else {
                    msg('<center><strong>Erro ao salvar dados!</strong></center>', 'erro');
                }
            }
        });
        return false;
    });

    // FUNÇÕES GERAIS
    function msg(msg, tipo) {
        var retorno = $("#retorno");
        var tipo = (tipo === 'sucesso') ? 'info' : (tipo === 'info') ? 'info' : (tipo === 'erro') ? 'danger' : (tipo === 'alerta') ? 'warning' : alert('INFORME MENSAGEM DE SUCESSO, ALERTA, ERRO E INFO');

        retorno.empty().slideUp('fast', function () {
            return $(this).html('<div class="alert alert-' + tipo + '" role="alert">' + msg + '</div>').slideDown('slow');
        });
        setTimeout(function () {
            retorno.slideUp('slow');
        }, 4000);
    }
    $('#telefoneEmpresa1').mask("(99) 99999-9999");
    $('#telefoneEmpresa2').mask("(99) 99999-9999");
	$('#faxEmpresa').mask("(99) 99999-9999");
    $('#cepEmpresa').mask("99.999-999");
});