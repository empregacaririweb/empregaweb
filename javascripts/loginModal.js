$(document).ready(function () {

    var janela = $("#customer_login_link");
    var conteudo = $('.modal-body');

    //FUNÇÃO PARA GERAR O MODAL DO LOGIN
    janela.click(function () {
        $.post('controlador/ajax/loginModal.php', {acao: 'form_modal_login'}, function (retorno) {
            $('#myModal').modal({backdrop: 'static'});
            conteudo.html(retorno);
        });
    });

    //FUNÇÃO PARA TESTAR O LOGIN DO USUARIO
    $("#myModal").on("submit", "form[name='form_modal_login']", function () {

        var form = $(this);
        var botao = form.find(':button');

        $.ajax({
            url: "controlador/ajax/controller.php",
            type: "POST",
            data: "acao=login&" + form.serialize(),
            beforeSend: function () {
                botao.attr('disabled', true);
                $("#loads").fadeIn('slow');
            },
            success: function (retorno) {
                botao.attr('disabled', false);
                $("#loads").fadeOut('slow');
                if (retorno === 'sucesso-c') {
                    $("#carregando").fadeOut(function () {
                        msg('<center><img src="imagens/gif-load.gif" id="img"/> <strong>Login efetuado com sucesso, Aguarde...</strong></center>', 'sucesso');
                        window.setTimeout(function () {
							$(location).attr('href', 'index.php');
                        }, 4000);
						
                    });
                } else if (retorno === 'sucesso-e') {
                    $("#carregando").fadeOut(function () {
                        msg('<center><img src="imagens/gif-load.gif" id="img"/> <strong>Login efetuado com sucesso, Aguarde...</strong></center>', 'sucesso');
                        window.setTimeout(function () {
							$(location).attr('href', 'empresa/index.php');
                        }, 4000);
						
                    });
                }else if (retorno === 'sucesso-b') {
                    $("#carregando").fadeOut(function () {
                        msg('<center><img src="imagens/gif-load.gif" id="img"/> <strong>Login efetuado com sucesso, Aguarde...</strong></center>', 'sucesso');
                        window.setTimeout(function () {
							$(location).attr('href', 'bloq.php');
                        }, 4000);
						
                    });
                } else if (retorno === 'erro') {
                    $('#carregando').fadeOut(function () {
                        msg('<center>E-mail ou Senha incorretos. Tente novamente.</center>', 'erro');
                    });
                } else if (retorno === 'empty') {
                    $('#carregando').fadeOut(function () {
                        msg('<center>Login e Senha devem ser preenchidos.</center>', 'info');
                    });
                }
            }
        });
        return false;
    });

    //FUNÇÕES DE ALERTA DE ERRO
    function msg(msg, tipo) {
        var retorno = $(".retorno");
        var tipo = (tipo === 'sucesso') ? 'success' : (tipo === 'alerta') ? 'warning' : (tipo === 'erro') ? 'danger' : (tipo === 'info') ? 'info' : alert('INFORME MENSAGEM DE SUCESSO, ALERTA, ERRO E INFO');

        retorno.empty().slideUp('fast', function () {
            return $(this).html('<div class="alert alert-' + tipo + '" role="alert">' + msg + '</div>').slideDown('slow');
        });
        setTimeout(function () {
            retorno.slideUp('slow');
        }, 4000);
    }
});