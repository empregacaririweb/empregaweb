<?php
session_start();
include_once "funcoes/conexao/conexao.php";
$pdo = conectar();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Catalogo Cariri</title>

        <!-- INCLUDE STYLESHEET -->
        <link rel="stylesheet" href="stylesheests/style.css">
        <link rel="stylesheet" href="stylesheests/bootstrap.min.css">
    </head>
    <body>

        <div id="page-wrapper">

            <!-- MODAL LOGIN -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                            <h4 class="modal-title" id="myModalLabel">Digite seus dados</h4>
                        </div>
                        <div class="modal-body"></div>              
                    </div>
                </div>
            </div>
        </div>


        <!-- INCLUDE JAVASCRIPTS -->
        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/loginModal.js"></script>
        <script src="javascripts/bootstrap.min.js"></script>
        <script src="javascripts/holder.js"></script>
        <script src="javascripts/scripts.site.js"></script>
    </body>
</html>