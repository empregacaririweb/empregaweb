<?php 
	$cargo = $_POST['cargo'];
	$cidade = $_POST['cidade'];
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Catalogo Cariri</title>

        <!-- INCLUDE STYLESHEET -->
        <link rel="stylesheet" href="stylesheests/style.css">
        <link rel="stylesheet" href="stylesheests/bootstrap.min.css">
    </head>
    <body>
        <!-- BEGIN HEADER -->
        <?php include "topo.php" ?>
        <!-- END HEADER -->

        <!-- BEGIN FEATURED PRODUTS -->
        <section id="wrapper-products">
            <div class="container">
                <div class="row">
                    <div class="section-title">
                        <div class="span6 title"><h2>Resultados:</h2></div>

                    </div>
                </div>
                <div class="row">
                    <div class="products">
					<?php
						include_once ('Funcoes/conexao/conexao.php');
						$pdo = conectar();
						if(empty($cargo) && empty($cidade)){
							$consulta = $pdo->query("SELECT * FROM v_vagas;");
						}
						else if(empty($cargo) && isset($cidade)){
							$consulta = $pdo->query("SELECT * FROM v_vagas where nm_cidade = '" . $cidade . "' ");
						}
						else if(empty($cidade) && isset($cargo)){
							$consulta = $pdo->query("SELECT * FROM v_vagas where nm_cargo = '" . $cargo . "' ");
						}
						else if(isset($cidade) && isset($cargo)){
							$consulta = $pdo->query("SELECT * FROM v_vagas where nm_cargo = '" . $cargo . "' and nm_cidade = '" . $cidade . "' ");
						}
						
						while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
							// aqui eu mostro os valores de minha consulta
							$id_vaga = $linha['id_vaga'];
							$nm_cargo = $linha['nm_cargo'];
							$qt_vaga = $linha['qt_vaga'];
							$nm_cidade = $linha['nm_cidade'];
							
					?>
                        <div class="span6">
                            <div class="product">
                                <img src="imagens/logo0.png" alt="product image" class="thumbnail">
                                <div class="product-details">
                                    <strong><?php print $nm_cargo;?></strong>
                                    <p class="new-price">Vagas: <strong><?php print $qt_vaga;?></strong></p>
                                    <p class="new-price">Cidade: <strong><?php print $nm_cidade;?></strong></p> 
                                </div>
                            </div>
                            
                        </div>
						<?php }?>
                       
                    </div>
                </div>
            </div>
        </section>
        <!-- END FEATURED PRODUTS -->

        <!-- BEGIN FOOTER -->
        <?php include "rodape.php" ?>
        <!-- END FOOTER -->


        <!-- INCLUDE JAVASCRIPTS -->
        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/bootstrap.min.js"></script>
        <script src="javascripts/holder.js"></script>
        <script src="javascripts/scripts.site.js"></script>
    </body>
</html>