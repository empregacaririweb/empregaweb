<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Catalogo Cariri</title>

        <!-- INCLUDE STYLESHEET -->
        <link rel="stylesheet" href="stylesheests/cadCandidato.css" />
        <link rel="stylesheet" href="stylesheests/style.css" />
        <link rel="stylesheet" href="stylesheests/bootstrap.min.css" />
        <style>
            body {
                font-family: Arial, Verdana, sans-serif;
                font-size: 13px;
            }
            .ui-autocomplete {
                padding: 0;
                list-style: none;
                background-color: #fff;
                width: 218px;
                border: 1px solid #B0BECA;
                max-height: 350px;
                overflow-x: hidden;
            }
            .ui-autocomplete .ui-menu-item {
                border-top: 1px solid #B0BECA;
                display: block;
                padding: 4px 6px;
                color: #353D44;
                cursor: pointer;
            }
            .ui-autocomplete .ui-menu-item:first-child {
                border-top: none;
            }
            .ui-autocomplete .ui-menu-item.ui-state-focus {
                background-color: #D5E5F4;
                color: #161A1C;
            }
        </style>

        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/jquery-ui.min.js"></script>
        <script src="javascripts/jquery.select-to-autocomplete.js"></script>
        <script type="text/javascript">
            (function ($) {
                $(function () {
                    $('select').selectToAutocomplete();
                });
            })(jQuery);
        </script>
        <link rel="stylesheet" href="stylesheests/jquery-ui.css" />
    </head>
    <body>

        <?php include "topo.php" ?>

        <div id="container">
            <center><h3 style="color: #0077b3;"><span style="border-bottom: solid 1px #0077b3;">Anuncie agora mesmo o seu Currículo Vitae no Emprega Cariri</span></h3></center><!-- fim do da frase de anúncio -->
            <div id="retorno"></div>
            <div id="curriculo">
                <div class="row">
                    <h4 style="color: #000000;"><strong>Dados pessoais:</strong></h4>
                    <form role="form" id="formCandidato" name="formCandidato" action="" method="POST">
                        <div class="form-group">
                            <label style="color: #000000;">Nome Completo</label>
                            <input type="text" name="nome" id="nome" class="form-control" autofocus="autofocus" />
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">E-mail</label>
                            <input type="email" name="email" id="email" class="form-control" required="required" />
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Telefone Celular</label>
                            <input type="text" name="telefone1" id="telefone1" class="form-control" maxlength="13" />
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Telefone Fixo <span style="color: #969696;">(Opcional)</span></label>
                            <input type="text" name="telefone2" id="telefone2" class="form-control" maxlength="13" />
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Endereço</label>
                            <input type="text" name="endereco" id="endereco" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Número Residêncial</label>
                            <input type="number" name="numero" id="numero" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Bairro</label>
                            <input type="text" name="bairro" id="bairro" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Cidade</label>
                            <select name="cidade" id="country-selector" class="cidade" autocorrect="off" autocomplete="off">
                                <option value="" selected="selected"></option>
                                <?php
                                $listaCidade = $pdo->prepare("SELECT * FROM cidade");
                                $listaCidade->execute();
                                $todasCidade = $listaCidade->fetchAll(PDO::FETCH_OBJ);
                                $contaCidade = $listaCidade->rowCount();
                                if ($contaCidade == 0) {
                                    ?>
                                    <option value=""></option>
                                    <?php
                                } else {
                                    foreach ($todasCidade as $dadosCidade):
                                        ?>
                                        <option value="<?php echo $dadosCidade->id_cidade; ?>"><?php echo $dadosCidade->nm_cidade; ?></option>		
                                        <?php
                                    endforeach;
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">CEP</label>
                            <input type="text" name="cep" id="cep" class="form-control" maxlength="8" />
                        </div>
						
						<div class="form-group">
                            <label style="color: #000000;">Senha</label>
                            <input type="password" name="senha" id="senha" class="form-control"/>
                        </div>
						
						<div class="form-group">
                            <label style="color: #000000;">Sexo</label>
                            <input type="text" name="sexo" id="sexo" class="form-control"  maxlength="1"/>
                        </div>
						
						<div class="form-group">
                            <label style="color: #000000;">Escolaridade</label>
                            <input type="text" name="escolaridade" id="escolaridade" class="form-control"/>
                        </div>
						
						<div class="form-group">
                            <label style="color: #000000;">CPF</label>
                            <input type="text" name="cpf" id="cpf" class="form-control"/>
                        </div>
						
						<div class="form-group">
                            <label style="color: #000000;">RG</label>
                            <input type="text" name="rg" id="rg" class="form-control"/>
                        </div>
						
						<div class="form-group">
                            <label style="color: #000000;">Data de Nascimento</label>
                            <input type="text" name="dtNasc" id="dtNasc" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Plano</label>
                            <select name="plano" id="country-selector2" class="plano" autocorrect="off" autocomplete="off">
                                <option value="" selected="selected"></option>
                                <?php
                                $listaPlano = $pdo->prepare("SELECT * FROM plano");
                                $listaPlano->execute();
                                $todasPlano = $listaPlano->fetchAll(PDO::FETCH_OBJ);
                                $contaPlano = $listaPlano->rowCount();
                                if ($contaPlano == 0) {
                                    ?>
                                    <option value=""></option>
                                    <?php
                                } else {
                                    foreach ($todasPlano as $dadosPlano):
                                        ?>
                                        <option value="<?php echo $dadosPlano->id_plano; ?>"><?php echo $dadosPlano->nm_plano; ?></option>		
                                        <?php
                                    endforeach;
                                }
                                ?>
                            </select>
                        </div>
                        
                        <button class="btn btn-primary" type="submit" name="cadCurriculo">Continuar</button>
                    </form><!-- fim do formulário de cadastro da pessoa física -->
                </div>
            </div><!-- fim do curriculo que faz a intereção com o form -->
        </div><!-- fim do container -->

        <!-- BEGIN FOOTER -->
        <?php include "rodape.php" ?>
        <!-- END FOOTER -->


        <!-- INCLUDE JAVASCRIPTS -->
        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/cadastroCandidatoForm.js"></script>
        <script src="javascripts/jquery.maskedinput.min.js"></script>
        <script src="javascripts/bootstrap.min.js"></script>
        <script src="javascripts/holder.js"></script>
        <script src="javascripts/scripts.site.js"></script>
    </body>
</html>