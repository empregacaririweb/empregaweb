<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Catalogo Cariri</title>

        <!-- INCLUDE STYLESHEET -->
        <link rel="stylesheet" href="stylesheests/cadCandidato.css" />
        <link rel="stylesheet" href="stylesheests/style.css" />
        <link rel="stylesheet" href="stylesheests/bootstrap.min.css" />
        <style>
            body {
                font-family: Arial, Verdana, sans-serif;
                font-size: 13px;
            }
            .ui-autocomplete {
                padding: 0;
                list-style: none;
                background-color: #fff;
                width: 218px;
                border: 1px solid #B0BECA;
                max-height: 350px;
                overflow-x: hidden;
            }
            .ui-autocomplete .ui-menu-item {
                border-top: 1px solid #B0BECA;
                display: block;
                padding: 4px 6px;
                color: #353D44;
                cursor: pointer;
            }
            .ui-autocomplete .ui-menu-item:first-child {
                border-top: none;
            }
            .ui-autocomplete .ui-menu-item.ui-state-focus {
                background-color: #D5E5F4;
                color: #161A1C;
            }
        </style>

        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/jquery-ui.min.js"></script>
        <script src="javascripts/jquery.select-to-autocomplete.js"></script>
        <script type="text/javascript">
            (function ($) {
                $(function () {
                    $('select').selectToAutocomplete();
                });
            })(jQuery);
        </script>
        <link rel="stylesheet" href="stylesheests/jquery-ui.css" />
    </head>
    <body>

        <?php include "topo.php" ?>

        <div id="container">
            <center><h3 style="color: #0077b3;"><span style="border-bottom: solid 1px #0077b3;">Anuncie agora mesmo o seu Currículo Vitae no Emprega Cariri</span></h3></center><!-- fim do da frase de anúncio -->
            <div id="retorno"></div>
            <div id="curriculo">
                <div class="row">
					<p style="color: #000000;"><a href="cadastro_capacitacao.php" style="color: #000000;">Capacitações > </a><a href="cadastro_experiencia.php" style="color: #000000;"><strong>Experiências Profissionais</strong></a></p>
                    <h4 style="color: #000000;"><strong>Experiências Profissionais:</strong></h4>
                    <form role="form" id="formExperiencia" name="formExperiencia" action="" method="POST">
					<div class="form-group">
                            <input type="hidden" name="idPessoaExperiencia" id="idPessoaExperiencia" value="<?php print $_SESSION['id_pessoa'];?>" class="form-control" required="required"/>
                        </div>
                        <div class="form-group">
                            <label style="color: #000000;">Cargo</label>
                            <input type="text" name="cargoExperiencia" id="cargoExperiencia" class="form-control" autofocus="autofocus" required="required"/>
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Empresa</label>
                            <input type="text" name="empresaExperiencia" id="empresaExperiencia" class="form-control" required="required" />
                        </div>

                        <div class="form-group">
                            <label style="color: #000000;">Período</label>
                            <input type="text" name="periodoExperiencia" id="periodoExperiencia" class="form-control" required="required"/>
                        </div>
						<input type="submit" class="btn btn-primary" value="< Anterior" onclick="location. href= 'cadastro_capacitacao.php' ">
						<button class="btn btn-primary" type="submit" name="cadCurriculo">Cadastrar</button>
						<input type="submit" class="btn btn-primary" value="Concluir" onclick="location. href= 'index.php' ">
                    </form><!-- fim do formulário de cadastro da pessoa física -->
                </div>
            </div><!-- fim do curriculo que faz a intereção com o form -->
        </div><!-- fim do container -->

        <!-- BEGIN FOOTER -->
        <?php include "rodape.php" ?>
        <!-- END FOOTER -->


        <!-- INCLUDE JAVASCRIPTS -->
        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/cadastroExperienciaForm.js"></script>
        <script src="javascripts/jquery.maskedinput.min.js"></script>
        <script src="javascripts/bootstrap.min.js"></script>
        <script src="javascripts/holder.js"></script>
        <script src="javascripts/scripts.site.js"></script>
    </body>
</html>