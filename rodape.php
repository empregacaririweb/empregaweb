<html>
    <head>
    </head>
    <body>
        <!-- BEGIN FOOTER -->
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="span6">
                        <p>Copyright &copy; 2015, Emprega Cariri - Todos os Direitos Reservados </p>
                    </div>
                    <div class="span6">
                        <ul class="inline pull-right">
                            <li id="texto">Acompanhe nas redes sociais</li>
                            <li id="facebook"><a href="#"><img src="imagens/icon-facebook.png" alt="facebook icon"></a></li>
                            <li id="twitter"><a href="#"><img src="imagens/icon-twitter.png" alt="twitter icon"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END FOOTER -->
    </body>
</html>