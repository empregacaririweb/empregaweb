<html>
    <head>
    </head>
    <body>
        <!-- BEGIN SLIDER -->
        <section id="wrapper-slider">
            <div id="myCarousel" class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="imagens/1.png" alt="produto um">
                        <div class="carousel-caption">
                            <h4>O emprego dos seus Sonhos</h4>
                            <p>O emprego dos seus sonhos está no Emprega Cariri, não perca tempo, cadastre-se agora mesmo</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="imagens/2.png" alt="produto dois">
                        <div class="carousel-caption">
                            <h4>Saia na frente</h4>
                            <p>Com o Emprega Cariri você sempre estará por dentro com o que o mercado lhe oferece</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="imagens/3.png" alt="produto três">
                        <div class="carousel-caption">
                            <h4>Emprego a vista</h4>
                            <p>O Emprega Cariri oferece ferramentas para lhe auxiliar a obter seu emprego</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
            </div>
        </section>
        <!-- END SLIDER -->
    </body>
</html>