<!doctype html>
<?php
	if(isset($_GET['logoff'])){ // Verificar se foi solicitado logoff
		unset ($_GET['logoff']);// Destroi $_GET['logoff']
		logoff(); // Chama função para logoff
	}
?>
<html>

    <head>
        <meta charset="UTF-8" />
        <style>
			.login {
				margin-top: -100px;
				margin-left: 73%;
				
			}
			.cv {
				margin-top: -100px;
				margin-left: 81.5%;
			}
			#customer_register_link, #customer_login_link {
				text-decoration: none;
				
			}
			body {
				font-family: Arial, Verdana, sans-serif;
				font-size: 13px;
			}
			.ui-autocomplete {
				padding: 0;
				list-style: none;
				background-color: #fff;
				width: 218px;
				border: 1px solid #B0BECA;
				max-height: 350px;
				overflow-x: hidden;
			}
			.ui-autocomplete .ui-menu-item {
				border-top: 1px solid #B0BECA;
				display: block;
				padding: 4px 6px;
				color: #353D44;
				cursor: pointer;
			}
			.ui-autocomplete .ui-menu-item:first-child {
				border-top: none;
			}
			.ui-autocomplete .ui-menu-item.ui-state-focus {
				background-color: #D5E5F4;
				color: #161A1C;
			}
        </style>
        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/jquery-ui.min.js"></script>
        <script src="javascripts/jquery.select-to-autocomplete.js"></script>
        <script type="text/javascript">
			(function($) {
				$(function() {
					$('select').selectToAutocomplete();
				});
			})(jQuery);
        </script>
        <link rel="stylesheet" href="../stylesheests/jquery-ui.css" />
        <link rel="stylesheet" href="stylesheests/topo.css"/>
    </head>
    <body>

        <?php
		
		include 'painel.php';
		function logoff(){ //Função responsável pelo logoff onde o mesmo limpa os dados da sessão
			session_start();
			session_destroy();
		}
        if(isset($_SESSION['NM_CLIENTE'])){
			$bt1 = "sair";
			$bt2 = "conta";
			$id_login = "";
			$href_bt1 = "?logoff";
			$href_bt2 = "candidato/index.php";
			}else{
			$bt1 = "login";
			$bt2 = "cadastro";
			$id_login = "customer_login_link";
			$href_bt1 = "#";
			$href_bt2 = "#abrirCadastroModal";
			
		}
?>

        <!-- BEGIN HEADER -->
        <header id="header">
            <div class="container">
                <div class="row">

                    <div class="span3">
                        <a href="index.php">
                            <img src="imagens/logo-catalogocariri.png" alt="Emprega Cariri" id="logo">
                        </a>
                    </div>

                    <div class="span3">
                        <form class="form-search" action="pesquisa.php" method="post" role="form" name="form-input">
                            <div class="input-append">
                                <select name="cargo" id="country-selector" autocorrect="off" autocomplete="off" placeholder="Digite cargo ou área profissional" style="height: 30px; width: 300px; margin:15px 0 0 20px;">
                                    <option value="" selected="selected"></option>
                                    <?php
	                                    $listaVaga = $pdo->prepare("SELECT * FROM cargo");
	                                    $listaVaga->execute();
	                                    $todasVaga = $listaVaga->fetchAll(PDO::FETCH_OBJ);
	                                    $contaVaga = $listaVaga->rowCount();
	                                    if ($contaVaga == 0) {
                                        ?>
                                        <option value=""></option>
                                        <?php
										} else {
										foreach ($todasVaga as $dadosVaga):
                                            ?>
                                            <option value="<?php echo $dadosVaga -> idCARGO; ?>"><?php echo $dadosVaga -> NM_CARGO; ?></option>		
                                            <?php
											endforeach;
											}
                                    ?>
                                </select>

                                <select name="cidade" id="country-selector" autocorrect="off" autocomplete="off" placeholder="Todo o Cariri" style="height: 30px; width: 200px; margin:15px 0 0 20px;">
                                    <option value="" selected="selected"></option>
                                    <?php
	                                    $listaCidade = $pdo->prepare("SELECT * FROM cidade");
	                                    $listaCidade->execute();
	                                    $todasCidade = $listaCidade->fetchAll(PDO::FETCH_OBJ);
	                                    $contaCidade = $listaCidade->rowCount();
	                                    if ($contaCidade == 0) {
                                        ?>
                                        <option value=""></option>
                                        <?php
										} else {
										foreach ($todasCidade as $dadosCidade):
                                            ?>
                                            <option value="<?php echo $dadosCidade -> nm_cidade; ?>"><?php echo $dadosCidade -> nm_cidade; ?></option>		
                                            <?php
											endforeach;
											}
                                    ?>
                                </select>
                                <button type="submit" class="btn" style="height: 40px; margin-top: 15px"><strong>Buscar Vagas</strong></button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="login">
                    <div class="toolbar clearfix">
                        <ul class="unstyled">
                            <li class="customer-links">
                                <a href="<?php echo $href_bt1; ?>" id="<?php echo $id_login; ?>"><img src="<?php echo"imagens/".$bt1."1.png"; ?>" alt="login" border="0" onmouseover="this.src='<?php echo"imagens/".$bt1."2.png"; ?>'" onmouseout="this.src='<?php echo"imagens/".$bt1."1.png"; ?>'"/></a>
								</li>
                        </ul>
                    </div>
                </div>
				<div class="cv">
                    <div class="toolbar clearfix">
                        <ul class="unstyled">
                            <li class="customer-links">
                                <a href="<?php echo $href_bt2; ?>"><img src="<?php echo"imagens/".$bt2."1.png"; ?>" alt="cadastro" border="0" onmouseover="this.src='<?php echo"imagens/".$bt2."2.png"; ?>'" onmouseout="this.src='<?php echo"imagens/".$bt2."1.png"; ?>'"/></a>
                            </li>
                        </ul>
                    </div>
                </div>
				<div id="abrirCadastroModal" class="janelaModal">
					<div>
						<div class="cabecarioModal">
							<h2>Selecione o Seu Cadastro</h2>
							<a href="#fechar" title="Fechar" class="fechar">X</a>
						</div>
						<div class="conteudoModal">
							<a href="cadastro_candidato.php" id="cand"><img src="<?php echo"imagens/cand1.png"; ?>" alt="Cadastro Candidato" border="0" onmouseover="this.src='<?php echo"imagens/cand2.png"; ?>'" onmouseout="this.src='<?php echo"imagens/cand1.png"; ?>'"/></a>
							<a href="cadastro_empresa.php" id="emp"><img src="<?php echo"imagens/emp1.png"; ?>" alt="Cadastro Empresa" border="0" onmouseover="this.src='<?php echo"imagens/emp2.png"; ?>'" onmouseout="this.src='<?php echo"imagens/emp1.png"; ?>'"/></a>
						</div>

					</div>
				</div>
            </div>
		<?php if(isset($_SESSION['NM_CLIENTE'])){
			$nome = $_SESSION['NM_CLIENTE'];
			$login_bar = "<div id='login_usuario'><label>Olá <b>$nome</b> seja bem-vindo ao <b>Emprega Cariri</b></label></div>";
		}else{
			$nome = "Usuário";
			$login_bar = " ";
		}
?>
		<?php echo $login_bar; ?>
		</header>
        <!-- END HEADER -->

        <!-- BEGIN NAV -->
		
        <nav id="nav">
            <div class="container">
                <div class="row">
                    <ul class="inline">
                        <li class="active">
                            <a href="index.php">Home</a>
                        </li>
						<li>
                            <a href="vagas.php">Vagas</a>
                        </li>
                        <li>
                            <a href="#">Cursos</a>
                        </li>

                        <li>
                            <a href="vagas.php">Dicas</a>
                        </li>
						<li>
                            <a href="vagas.php">Contato</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END NAV -->
    </body>
</html>