﻿

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Emprega Cariri</title>

        <!-- INCLUDE STYLESHEET -->
        <link rel="stylesheet" href="../stylesheests/style.css">
        <link rel="stylesheet" href="../stylesheests/bootstrap.min.css">
		
    </head>
    <body>
        <!-- BEGIN HEADER -->
        <?php include "topo.php" ?>
        <!-- END HEADER -->

        <!-- BEGIN FEATURED PRODUTS -->
        <section id="wrapper-products">
            <div class="container">
                <div class="row">
                    <div class="section-title">
                        <div class="span6 title"><h2>Cargo </h2></div>

                    </div>
                </div>
                <div class="row">
                    <div class="products">
					<?php
						include_once ('funcoes/conexao/conexao.php');
						$pdo = conectar();
						$consulta = $pdo->query("SELECT * FROM v_vagas ORDER BY idVAGAS desc;");
						while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
							// aqui eu mostro os valores de minha consulta
							$nm_cargo = $linha['NM_CARGO'];
							$ds_setor = $linha['DS_SETOR'];
							$nm_cidade = $linha['NM_CIDADE'];
							//}
					?>
                        <div class="span6">
                            <div class="product">
                                <img src="imagens/logo0.png" alt="product image" class="thumbnail">
                                <div class="product-details">
                                    <strong><?php print $nm_cargo;?></strong>
                                    <p class="new-price">Cidade: <strong><?php print $nm_cidade;?></strong></p>
									<br/>
									<a href="#" name="CV"><img src="imagens/candidatar1.png" alt="candidatar" border="0" onmouseover="this.src='imagens/candidatar2.png'" onmouseout="this.src='imagens/candidatar1.png'"/></a>
                                </div>
                            </div>
                            
                        </div>
						<?php }?>
                       
                    </div>
                </div>
            </div>
        </section>
        <!-- END FEATURED PRODUTS -->

        <!-- BEGIN FOOTER -->
        <?php include "rodape.php" ?>
        <!-- END FOOTER -->


        <!-- INCLUDE JAVASCRIPTS -->
        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/bootstrap.min.js"></script>
        <script src="javascripts/holder.js"></script>
        <script src="javascripts/scripts.site.js"></script>
    </body>
</html>