<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Catalogo Cariri</title>

        <!-- INCLUDE STYLESHEET -->
        <link rel="stylesheet" href="stylesheests/style.css" />
        <link rel="stylesheet" href="stylesheests/bootstrap.min.css" />
    </head>
    <body>
        <!-- BEGIN HEADER -->
        <?php include "topo.php" ?>
        <!-- END HEADER -->

        <!-- BEGIN SLIDER -->
        <?php include "slide.php" ?>
        <!-- END SLIDER -->

        <!-- BEGIN FEATURED PRODUTS -->
        <section id="wrapper-products">
            <div class="container">
                <div class="row">
                    <div class="section-title">
                        <div class="span6 title"><h2>Destaques </h2></div>

                    </div>
                </div>
                <div class="row">
                    <div class="products">
                        <div class="span6">
                            <div class="product">
                                <img src="imagens/logo1.png" alt="product image" class="thumbnail">
                                <div class="product-details">
                                    <strong>Programador Web</strong>
                                    <p class="price">Salário: R$ 6.000,00</p>
                                    <p class="new-price">Vagas: <strong>2</strong></p>
                                    <p class="new-price">Cidade: <strong>Juazeiro do Norte</strong></p>
                                    <small><a href="#">Mais detalhes</a><span class=" icon-chevron-right"></span></small> 
                                </div>
                            </div>
                            <div class="product">
                                <img src="imagens/logo2.png" alt="product image" class="thumbnail">
                                <div class="product-details">
                                    <strong>Contador</strong>
                                    <p class="price">Salário: R$ 4.000,00</p>
                                    <p class="new-price">Vagas: <strong>1</strong></p>
                                    <p class="new-price">Cidade: <strong>Juazeiro do Norte</strong></p>
                                    <small><a href="#">Mais detalhes</a><span class=" icon-chevron-right"></span></small> 
                                </div>
                            </div>
                            <div class="product">
                                <img src="imagens/logo3.png" alt="product image" class="thumbnail">
                                <div class="product-details">
                                    <strong>Programador JSF</strong>
                                    <p class="price">Salário: R$ 2.000,00</p>
                                    <p class="new-price">Vagas: <strong>4</strong></p>
                                    <p class="new-price">Cidade: <strong>Brejo Santo</strong></p>
                                    <small><a href="#">Mais detalhes</a><span class=" icon-chevron-right"></span></small> 
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="product">
                                <img src="imagens/logo4.png" alt="product image" class="thumbnail">
                                <div class="product-details">
                                    <strong>DBA</strong>
                                    <p class="price">Salário: R$ 3.000,00</p>
                                    <p class="new-price">Vagas: <strong>2</strong></p>
                                    <p class="new-price">Cidade: <strong>Mauriti</strong></p>
                                    <small><a href="#">Mais detalhes</a><span class=" icon-chevron-right"></span></small> 
                                </div>
                            </div>
                            <div class="product">
                                <img src="imagens/logo5.png" alt="product image" class="thumbnail">
                                <div class="product-details">
                                    <strong>Aux. de Escritório</strong>
                                    <p class="price">Salário: R$ 1.000,00</p>
                                    <p class="new-price">Vagas: <strong>7</strong></p>
                                    <p class="new-price">Cidade: <strong>Barbalha</strong></p>
                                    <small><a href="#">Mais detalhes</a><span class=" icon-chevron-right"></span></small> 
                                </div>
                            </div>
                            <div class="product">
                                <img src="imagens/logo6.png" alt="product image" class="thumbnail">
                                <div class="product-details">
                                    <strong>Doméstica</strong>
                                    <p class="price">Salário: R$ 900,00</p>
                                    <p class="new-price">Vagas: <strong>1</strong></p>
                                    <p class="new-price">Cidade: <strong>Crato</strong></p>
                                    <small><a href="#">Mais detalhes</a><span class=" icon-chevron-right"></span></small> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END FEATURED PRODUTS -->

        <!-- BEGIN FOOTER -->
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="span6">
                        <p>Copyright &copy; 2015, Emprega Cariri - Todos os Direitos Reservados </p>
                    </div>
                    <div class="span6">
                        <ul class="inline pull-right">
                            <li>Acompanhe nas redes sociais</li>
                            <li><a href="#"><img src="imagens/icon-facebook.png" alt="facebook icon"></a></li>
                            <li><a href="#"><img src="imagens/icon-twitter.png" alt="twitter icon"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END FOOTER -->


        <!-- INCLUDE JAVASCRIPTS -->
        <script src="javascripts/jquery.min.js"></script>
        <script src="javascripts/bootstrap.min.js"></script>
        <script src="javascripts/holder.js"></script>
        <script src="javascripts/scripts.site.js"></script>
    </body>
</html>